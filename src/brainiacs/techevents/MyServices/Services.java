/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.MyServices;

import brainiacs.techevents.crud.Password;
import brainiacs.techevents.entities.Utilisateur;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Services {
     public List<Utilisateur> rechercherUtilisateur (int id){
        
        String requete="select * FROM membres where id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        
        ArrayList<Utilisateur> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setInt(1,id);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Utilisateur u = new Utilisateur();
                u.setId(rs.getInt(1));
                u.setNom(rs.getString(2));
                u.setPrenom(rs.getString(3));
                u.setUsername(rs.getString(4));
                u.setMail(rs.getString(5));
                u.setMot_de_passe(rs.getString(6));
                u.setDate_naissance(rs.getString(7));
                u.setLocalisation(rs.getString(8));
                u.setNum_tel(rs.getString(9));
                u.setCategories(rs.getString(10));
           
                myList.add(u); 
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myList;
    }
    
    public static Boolean verifierpassword(String pword, String uname) {
        String s1 = "";
        String req = "Select mot_de_passe from membres where username= ?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            preparedStatement.setString(1, uname);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                s1 = resultSet.getString(1);
               // String s2=uname+"{"+s1+"}";
 
                  
              
                System.out.println("ili 3malnelou deccryptage==>"+Password.checkPassword(pword,s1));
                  //   System.out.println(uname);
                   System.out.println(s1);
                 
         
                if ( Password.checkPassword(pword,s1)) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;

    }
    
     public boolean chercherUtilisateurBylogin(String s) {
        Utilisateur user = null;
        String req = "select * from membres where username =?";
        PreparedStatement preparedStatement;
        try {
         preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                  user = new Utilisateur(
                        resultSet.getInt("id"),
                        resultSet.getString("nom"),
                        resultSet.getString("prenom"),
                        resultSet.getString("username"),
                        resultSet.getString("mail"),
                        resultSet.getString("mot_de_passe"),
                        resultSet.getString("date_naissance"),
                        resultSet.getString("localisation"),
                        resultSet.getString("num_tel"),
                        resultSet.getString("categories"));
                    
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (user == null) {
            return false;
        }
        return true;
    }
     public String Gettype(String s) {
        String s1 = "";
        String req = "select mot_de_passe from membres where username =?";
        PreparedStatement preparedStatement;
        try {
          preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                s1 = resultSet.getString("mot_de_passe");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return s1;
    }
     public Utilisateur chercherUtilisateurByUsername(String s) {
        Utilisateur user = null;
     
        PreparedStatement preparedStatement;
        try {
         preparedStatement = MyConnection.getInstance().getCnx().prepareStatement("select * from membres where username =?");
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                  user = new Utilisateur(
                        resultSet.getInt("id"),
                        resultSet.getString("nom"),
                        resultSet.getString("prenom"),
                        resultSet.getString("username"),
                        resultSet.getString("mail"),
                        resultSet.getString("mot_de_passe"),
                        resultSet.getString("date_naissance"),
                        resultSet.getString("localisation"),
                        resultSet.getString("num_tel"),
                        resultSet.getString("categories"));
                    
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (user == null) {
            return null;
        }
        return user;
    }
     public static boolean chercherUtilisateurByMail(String s) {
        Utilisateur user = null;
        String req = "select * from membres where mail =?";
        PreparedStatement preparedStatement;
        try {
         preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                  user = new Utilisateur(
                        resultSet.getInt("id"),
                        resultSet.getString("nom"),
                        resultSet.getString("prenom"),
                        resultSet.getString("username"),
                        resultSet.getString("mail"),
                        resultSet.getString("mot_de_passe"),
                        resultSet.getString("date_naissance"),
                        resultSet.getString("localisation"),
                        resultSet.getString("num_tel"),
                        resultSet.getString("categories"));
                    
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (user == null) {
            return false;
        }
        return true;
    }
}
