/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.MyServices;

import brainiacs.techevents.connexion.MyConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author User
 */
public class Adminservices {
    public static int get_Number_User() {
        
        int Message_Number = 989898;
        Connection connection;
        PreparedStatement ps;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/techevents", "root", "");
            //String req = "SELECT COUNT(categories) FROM `membres` WHERE `username` = ?";
            //PreparedStatement preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            ps = connection.prepareStatement("SELECT COUNT(categories) FROM `membres` WHERE `categories` = ?");
            ps.setString(1, "Simple utilisateur");
          

            ResultSet result = ps.executeQuery();
            while (result.next()) {
                Message_Number = result.getInt(1);
            }
            return Message_Number;
        } catch (Exception e) {
            System.out.println("Error on DB connection");
            System.out.println(e.getMessage());
        }
        return Message_Number;
    }
    public static int get_Number_Etab() {
        
        int Message_Number = 989898;
        Connection connection;
        PreparedStatement ps;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/techevents", "root", "");
            //String req = "SELECT COUNT(categories) FROM `membres` WHERE `username` = ?";
            //PreparedStatement preparedStatement = MyConnection.getInstance().getCnx().prepareStatement(req);
            ps = connection.prepareStatement("SELECT COUNT(categories) FROM `membres` WHERE `categories` = ?");
            ps.setString(1, "Etablissement");
          

            ResultSet result = ps.executeQuery();
            while (result.next()) {
                Message_Number = result.getInt(1);
            }
            return Message_Number;
        } catch (Exception e) {
            System.out.println("Error on DB connection");
            System.out.println(e.getMessage());
        }
        return Message_Number;
    }
}
