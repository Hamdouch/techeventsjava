/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import static brainiacs.techevents.controller.GActualiteController.stageModifier;
import brainiacs.techevents.crud.GestionActualite;
import brainiacs.techevents.entities.Actualite;
import com.jfoenix.controls.JFXButton;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ActualiteForumController implements Initializable {
   
    
    @FXML
    private TextField RechercheTextField;
    @FXML
    private TableView<Actualite> TableViewActualite;
     static Actualite selectionnedAct;
ObservableList<Actualite> dataActualitelist;
    @FXML
    private AnchorPane panefornode;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
          stageModifier = new Stage();
   
        
        
          TableColumn<Actualite, Date> dateColumn = new TableColumn<>("date");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateColumn.setStyle("-fx-alignment: CENTER;");
        
         TableColumn<Actualite, String> screenshotColumn = new TableColumn<>("PHOTO");
        screenshotColumn.setCellValueFactory(new PropertyValueFactory<>("pictures"));
        screenshotColumn.setStyle("-fx-alignment: CENTER;");
        
         TableColumn<Actualite, String> titreColumn = new TableColumn<>("Titre");
        titreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));
        titreColumn.setStyle("-fx-alignment: CENTER;");
         Actualite a = new Actualite();
        GestionActualite GA = new GestionActualite();

        dataActualitelist = FXCollections.observableArrayList(GA.afficherToutesLesActualites());
     
        TableViewActualite.setItems(dataActualitelist);
        TableViewActualite.getColumns().addAll(screenshotColumn,titreColumn,dateColumn);
          addButtonConsulterToTable();
        
        
         //***********************ajout photo dans la table 
         Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>> cellFactoryImage
                = //
                new Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>>() {
            @Override
            public TableCell call(final TableColumn<Actualite, String> param) {
                final TableCell<Actualite, String> cell = new TableCell<Actualite, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            try {
                                ImageView imagev = new ImageView(new Image(new FileInputStream(item)));
                                imagev.setFitHeight(120);
                                imagev.setFitWidth(200);
                                setGraphic(imagev);
                                setText(null);
                                //System.out.println(item);
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                };
                
                cell.setOnMouseClicked((MouseEvent event2)
                        -> {
                    if (event2.getClickCount() == 1) {
                        if (TableViewActualite.getSelectionModel().getSelectedItem() != null && !TableViewActualite.getSelectionModel().getSelectedItem().getPictures().contains("null")) {
                            Stage window = new Stage();
//
                            window.setMinWidth(250);
                            ImageView imagevPOPUP;
                            try {
                                imagevPOPUP = new ImageView(new Image(new FileInputStream( TableViewActualite.getSelectionModel().getSelectedItem().getPictures())));
                                imagevPOPUP.setFitHeight(576);
                            imagevPOPUP.setFitWidth(1024);
                            
                            VBox layout = new VBox(10);
                            layout.getChildren().addAll(imagevPOPUP);
                            layout.setAlignment(Pos.CENTER);

                            //Display window and wait for it to be closed before returning
                            Scene scene = new Scene(layout);
                            window.setScene(scene);
                            window.show();
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                           

                        }
                    }
                });
                
                
                return cell;
            }
        };

       screenshotColumn.setCellFactory(cellFactoryImage);
        

    

    
        
      


        
    }
    
    
    
    private void addButtonConsulterToTable() {
        
           TableColumn<Actualite, Void> colBtn = new TableColumn("Consulter les Actualités");

        Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>> cellFactory = new Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>>() {
            @Override
            public TableCell<Actualite, Void> call(final TableColumn<Actualite, Void> param) {
                final TableCell<Actualite, Void> cell = new TableCell<Actualite, Void>() {

                    private final JFXButton btn = new JFXButton("PLUS");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            selectionnedAct = getTableView().getItems().get(getIndex());

                            try {

                                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ActualiteConsulter.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }   

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        TableViewActualite.getColumns().add(colBtn);
        
        
        
        
    }

    @FXML
    private void listRecherche() {
         ArrayList arrayList = null;
        GestionActualite GA = new GestionActualite();

       //recherche avancée
            arrayList = (ArrayList) GA.rechercheActualite("Tout", RechercheTextField.getText());
        

        //("Categories", "Titre" ,"Description","User","Tout");
        ObservableList observableList = FXCollections.observableArrayList(arrayList);
        TableViewActualite.setItems(observableList);
    }

     private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
}
