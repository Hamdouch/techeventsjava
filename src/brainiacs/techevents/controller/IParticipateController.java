/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.IOneShowEventController;
import com.jfoenix.controls.JFXButton;
import brainiacs.techevents.crud.EventCrud;
import brainiacs.techevents.crud.ParticipantCrud;
import brainiacs.techevents.entities.Participant;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IParticipateController implements Initializable {
       @FXML
    private AnchorPane panefornode;
    @FXML
    private TextField pnom;
    @FXML
    private TextField pprenom;
    @FXML
    private TextField pcin;
    @FXML
    private TextField pmail;
    @FXML
    private TextField ptlph;
    @FXML
    private Button pbtn;
    @FXML
    private Label psucces;
    @FXML
    private JFXButton partretour;
    @FXML
    private Label lbnomvide;
    @FXML
    private Label lbprenomvide;
    @FXML
    private Label lbcinvide;
    @FXML
    private Label lbmailvide;
    @FXML
    private Label lbtlphvide;
    String nomvide="";
    String prenomvide="";
    String cinvide="";
    String mailvide="";
    String tlphvide="";
    boolean test=false;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void insertParticipant(ActionEvent event) {
        Participant p = new Participant();
            
            p.setCin(Integer.parseInt(pcin.getText()));
            p.setNom(pnom.getText());
            p.setPrenom(pprenom.getText());
            p.setMail(pmail.getText());
            p.setTlph(Integer.parseInt(ptlph.getText()));
            
            if(pnom.getText().equals(""))
        {
            test=true;
            nomvide=" Ce champs est vide !! "; 
        }
         if(pprenom.getText().equals(""))
        {test=true;
            prenomvide=" Ce champs est vide !! ";
        }
       /* if(pcin.getText().equals(""))
        {test=true;
        cinvide=" Ce champs est vide !! ";}*/ // je ne pas faire le controle saisie avec un type contre String
         if(pmail.getText().equals(""))
        {test=true;
            mailvide=" Ce champs est vide !! ";
        }
         /*if(ptlph.getText().equals(""))
        {test=true;
            tlphvide=" Ce champs est vide !! ";
        }*/
        if(test)
        {  
            lbnomvide.setText(nomvide);
            lbprenomvide.setText(prenomvide);
           // lbcinvide.setText(cinvide);
            lbmailvide.setText(mailvide);
           // lbtlphvide.setText(tlphvide);
        
        
        }
        else
        { 
            ParticipantCrud pc = new ParticipantCrud();
            pc.ajouterParticipant(p);
            EventCrud ec=new EventCrud();
            ec.incrParticipant(IShowEventController.trecup);
           // FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IPay.fxml"));
            
        try {
           /// Parent root = loader.load();
          //  IPayController rc = loader.getController();
            
           // psucces.getScene().setRoot(root);
           // rc.psucces.setText("Votre participation a été effectuée avec succès!");
          setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IPay.fxml"))));
            
           
            
         } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }}
    
    }

    @FXML
    private void backEvent(ActionEvent event) {
     try {
           /* FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"));

            Parent root= loader.load();
            IOneShowEventController rc= loader.getController(); 
         partretour.getScene().setRoot(root); */
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"))));
           
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
  private void setNode(Node node) {
        panefornode.getChildren().clear();
       panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
  
          
     
    
}
