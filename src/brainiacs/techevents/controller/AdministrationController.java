/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import brainiacs.techevents.MyServices.Adminservices;
import brainiacs.techevents.controller.InscriController;
import brainiacs.techevents.controller.ProfilController;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import brainiacs.techevents.connexion.MyConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.textfield.TextFields;

/**
 *
 * @author User
 */
 
public class AdministrationController implements Initializable {

    public static Utilisateur ut;
        @FXML
    private AnchorPane homepage;
        
    @FXML
    private TableColumn<Utilisateur, Integer> columnid;
    @FXML
    private TableColumn<Utilisateur, String> columnnom;
    @FXML
    private TableColumn<Utilisateur, String> columnprenom;
    @FXML
    private TableColumn<Utilisateur, String> columnusername;
    @FXML
    private TableColumn<Utilisateur, String> columnmail;
    @FXML
    private TableColumn<Utilisateur, String> columnpwd;
    @FXML
    private TableColumn<Utilisateur, String> columndate;
    @FXML
    private TableColumn<Utilisateur, String> columnlocalisation;
    @FXML
    private TableColumn<Utilisateur, String> columnnum;
    @FXML
    private TableColumn<Utilisateur, String> columncat;

    ObservableList<Utilisateur> dataUtilisateurlist;
    @FXML
    private TableView<Utilisateur> tableViewUtilisateur;
    @FXML
    private Button versajout;
    
     @FXML
    private Label membres;
     Adminservices adminService=new Adminservices();

    @FXML
    private TextField barsearch;
    public static String userrecup = "";
    public static String n = "";
    public static String pn = "";
    public static String nus = "";
    public static String m = "";
    public static String mp = "";
    public static String dn = "";
    public static String lo = "";
    public static String nu = "";
    public static String ca = "";
    public static String pht="";
    public static int idrec;
    @FXML
    private JFXButton utilisateur;
    @FXML
    private JFXButton Etablissement;
    
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        columnid.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnnom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnprenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        columnusername.setCellValueFactory(new PropertyValueFactory<>("username"));
        columnmail.setCellValueFactory(new PropertyValueFactory<>("mail"));
        columnpwd.setCellValueFactory(new PropertyValueFactory<>("mot_de_passe"));
        columndate.setCellValueFactory(new PropertyValueFactory<>("date_naissance"));
        columnlocalisation.setCellValueFactory(new PropertyValueFactory<>("localisation"));
        columnnum.setCellValueFactory(new PropertyValueFactory<>("num_tel"));
        columncat.setCellValueFactory(new PropertyValueFactory<>("categories"));

        addButtonDeleteToTable();
        addButtonUpdateToTable();

        Utilisateur a = new Utilisateur();
        UserCrud ac = new UserCrud();

        dataUtilisateurlist = FXCollections.observableArrayList(ac.afficherUtilisateur());

        tableViewUtilisateur.setItems(dataUtilisateurlist);

        List<String> test = new ArrayList<String>();

        String requete2 = "SELECT * FROM membres ";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            Statement st = myCNX.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete2);
            while (rs.next()) {
                Utilisateur u = new Utilisateur();
                u.setUsername(rs.getString(4));
                test.add(u.getUsername());
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        //System.out.println(test.size());
        for (String s : test) {
            String[] possiblewords = {s};
            TextFields.bindAutoCompletion(barsearch, possiblewords);
        }
        membres.setText(String.valueOf(Adminservices.get_Number_User()));
        
    }
    

    @FXML
    private void redirectionUtilisateur(ActionEvent event) {
     /*   try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Login.fxml"));
            Parent root = loader.load();
            LoginController rc = loader.getController();

            versajout.getScene().setRoot(root);   
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Login.fxml"))));
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }   */
    }

    private void addButtonDeleteToTable() {
        TableColumn<Utilisateur, Void> colBtn = new TableColumn("Supprimer Utilisateur");

        Callback<TableColumn<Utilisateur, Void>, TableCell<Utilisateur, Void>> cellFactory = new Callback<TableColumn<Utilisateur, Void>, TableCell<Utilisateur, Void>>() {
            @Override
            public TableCell<Utilisateur, Void> call(final TableColumn<Utilisateur, Void> param) {
                final TableCell<Utilisateur, Void> cell = new TableCell<Utilisateur, Void>() {

                    private final Button btn = new Button("Supprimer");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Utilisateur ut = getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Suppression");
                            alert.setHeaderText(null);
                            alert.setContentText("Vous voulez vraiment supprimer le compte d'utilisateur de username <" + ut.getUsername() + "> ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                UserCrud ac = new UserCrud();
                                ac.supprimerUtilisateur(ut.getId()); //supprimer T3amlet

                            }

                            try {

                              /*  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"));
                                Parent root = loader.load();
                                AdministrationController rc = loader.getController();
                                versajout.getScene().setRoot(root);   */
                                     setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"))));


                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tableViewUtilisateur.getColumns().add(colBtn);

    }

    private void addButtonUpdateToTable() {

        TableColumn<Utilisateur, Void> colBtn = new TableColumn("modifier Utilisateur");

        Callback<TableColumn<Utilisateur, Void>, TableCell<Utilisateur, Void>> cellFactory = new Callback<TableColumn<Utilisateur, Void>, TableCell<Utilisateur, Void>>() {
            @Override
            public TableCell<Utilisateur, Void> call(final TableColumn<Utilisateur, Void> param) {
                final TableCell<Utilisateur, Void> cell = new TableCell<Utilisateur, Void>() {

                    private final Button btn = new Button("modifier");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Utilisateur ut = getTableView().getItems().get(getIndex());
                            userrecup = ut.getUsername();
                            idrec = ut.getId();
                            n = ut.getNom();
                            pn = ut.getPrenom();
                            nus = ut.getUsername();
                            m = ut.getMail();
                            mp = ut.getMot_de_passe();
                            dn = ut.getDate_naissance();
                            lo = ut.getLocalisation();
                            nu = ut.getNum_tel();
                            ca = ut.getCategories();
                            pht= ut.getPhoto();

                            try {

                        /*        FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Profil.fxml"));
                                Parent root = loader.load();
                                ProfilController rc = loader.getController();
                                btn.getScene().setRoot(root);   */
                                 setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Profil.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tableViewUtilisateur.getColumns().add(colBtn);

    }

    @FXML
    private void search(KeyEvent event) {
        UserCrud UC = new UserCrud();
        ObservableList<Utilisateur> listuser = FXCollections.observableArrayList(UC.finduserbyUsername(barsearch.getText()));
        //Iterator<Utilisateur> i ;
       // while()
        tableViewUtilisateur.setItems(listuser);
        
    }
    
    private void setNode(Node node) {
        homepage.getChildren().clear();
        homepage.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(1));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    @FXML
    private void AfficherNbrUtilisateur(ActionEvent event) {
        membres.setText(String.valueOf(Adminservices.get_Number_User()));
    }

    @FXML
    private void AfficherNbrEtab(ActionEvent event) {
        membres.setText(String.valueOf(Adminservices.get_Number_Etab()));
    }
    
    
   
    
    
}
