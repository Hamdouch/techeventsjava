/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.GActualiteController;
import static brainiacs.techevents.controller.GActualiteController.selectionnedAct;
import static brainiacs.techevents.controller.GActualiteController.stageModifier;
import brainiacs.techevents.crud.GestionActualite;
import brainiacs.techevents.entities.Actualite;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class MDActualiteController implements Initializable {
     ObservableList<String> CategoriesStatuslist = FXCollections.observableArrayList("Evenement", "Article", "Formation","TechEvents");
    @FXML
    private JFXButton btnmdact;
    @FXML
    private JFXTextField texttitre;
    @FXML
    private JFXTextField textuser;
    @FXML
    private JFXTextArea textdesc;
    @FXML
    private ChoiceBox btncateg;
    @FXML
    private Label labelajresultat;
    @FXML
    private JFXButton retourbtn;
    @FXML
    private AnchorPane panemodif;
     @FXML
    private ImageView screenshotView;

    @FXML
    private JFXButton image;
    File selectedFile;
    private String path;

  String testtit = GActualiteController.selectionnedAct.getTitre();
  String testuser = GActualiteController.selectionnedAct.getUser();
  String testdesc = GActualiteController.selectionnedAct.getDescription();
  String testCategories = GActualiteController.selectionnedAct.getCategories();
  String testphoto = GActualiteController.selectionnedAct.getPictures();
   
    /**       
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        btncateg.setValue("Evenement");
        btncateg.setItems(CategoriesStatuslist);
        
          texttitre.setText(GActualiteController.selectionnedAct.getTitre());
          
          textuser.setText(GActualiteController.selectionnedAct.getUser());
           
          textdesc.setText(GActualiteController.selectionnedAct.getDescription());
          
          btncateg.setValue(GActualiteController.selectionnedAct.getCategories());
         
          path = GActualiteController.selectionnedAct.getPictures();
           
             image.setText("Modifier photo");
       
         try {
        
             screenshotView.setImage(new Image(new FileInputStream(path)));
         
         } catch (Exception ex) {
             System.out.println(ex.getMessage());
         }   


             
        screenshotView.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        // Dropping over surface
        screenshotView.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    path = null;
                    for (File file : db.getFiles()) {
                        path = file.getName();
                        selectedFile = new File(file.getAbsolutePath());
                        System.out.println("Drag and drop file done and path=" + file.getAbsolutePath());//file.getAbsolutePath()="C:\Users\X\Desktop\ScreenShot.6.png"
                        screenshotView.setImage(new Image("file:" + file.getAbsolutePath()));
//                        screenshotView.setFitHeight(150);
//                        screenshotView.setFitWidth(250);
                        //image.setText(path);
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });    

         try {
             screenshotView.setImage(new Image(new FileInputStream(path)));
         } catch (FileNotFoundException ex) {
             Logger.getLogger(MDActualiteController.class.getName()).log(Level.SEVERE, null, ex);
         }
    
    
        
        
        
        
        
    }
          

    @FXML
    private void modifactualite(ActionEvent event) {
    

      try{        

            //set les formation d'Actualite
         Actualite act = new Actualite();
            act.setTitre(texttitre.getText());
            act.setUser(textuser.getText());
            act.setDescription(textdesc.getText());
            act.setCategories(btncateg.getValue().toString());
            act.setPictures(path);
            
        //appel au CRUD pour utiliser l'Ajout

         
            
            
            
            
     if (act.getTitre().equals("") || act.getDescription().equals("") || act.getUser().equals("")) {
               
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/MDActualite.fxml"))));
          
                 Notifications n = Notifications.create()
                                .title("echec")
                                .text("il y a un champs ou plusieurs vide")
                                .graphic(null)
                                .position(Pos.TOP_CENTER)
                                .hideAfter(Duration.seconds(4));
                                n.showInformation();
              
            } 
             else {
                
                GestionActualite GA = new GestionActualite();
            
                GA.modifierActualite(act, GActualiteController.selectionnedAct.getId());
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));
                           Notifications n = Notifications.create()
                                .title("Succès")
                                .text("Modification d'actualite avec succès")
                                .graphic(null)
                                .position(Pos.TOP_CENTER)
                                .hideAfter(Duration.seconds(3));
                        n.showInformation();
                
           

            }   

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }       
        
        
        
        
        
 }
    

    @FXML
    private void retourgactualite(ActionEvent event) {
     
         try {
         
             
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));
             
        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        } 
      
       } 
    

    public TextField getTexttitre() {
        return texttitre;
    }

    public void setTexttitre(String texttitre) {
       // this.texttitre = texttitre;
         this.texttitre.setText(texttitre);
    }

    public TextField getTextuser() {
        return textuser;
    }

    public void setTextuser(String textusr) {
        this.textuser.setText(textusr);
    }

    public TextArea getTextdesc() {
        return textdesc;
    }

    public void setTextdesc(String textdesc) {
       this.textdesc.setText(textdesc);
    }

    public ChoiceBox getBtncateg() {
        return btncateg;
    }


    public void setBtncateg(ChoiceBox btncateg) {
        this.btncateg = btncateg;
    }

   
    private void setNode(Node node) {
        panemodif.getChildren().clear();
        panemodif.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
     @FXML
    void image(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home") + "\\Desktop"));
        fc.setTitle("Veuillez choisir l'image");
        fc.getExtensionFilters().addAll(
                //new FileChooser.ExtensionFilter("Image", ".jpg", ".png"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg")
        );
        selectedFile = fc.showOpenDialog(null);

        if (selectedFile != null) {

            path = selectedFile.getAbsolutePath();
            
            
//    
            image.setText(selectedFile.getName());
            Image imagea = new Image(selectedFile.toURI().toString());
           screenshotView.setImage(imagea) ;

        }

    }
    
    
}
