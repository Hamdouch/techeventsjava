/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import static brainiacs.techevents.controller.IAddEventController.imgPathRec;
import brainiacs.techevents.connexion.MyConnection;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IOneShowEventController implements Initializable {
    @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXTextField etabinesrslt;
    @FXML
    private JFXTextField titleinesrslt;
    @FXML
    private JFXTextField dateinesrslt;
    @FXML
    private JFXTextField prixinesrslt;
    @FXML
    private JFXTextField locinesrslt;
    @FXML
    private JFXTextArea descinesrslt;
    @FXML
    private JFXTextField catinesrslt;
    @FXML
    private JFXButton goparticipatebtn;
    @FXML
    private JFXButton returnshow;
    @FXML
    private ImageView imageEvent;
    @FXML
    private JFXTextField test;
    private String path;

    /**
     * Initializes the controller class.
     */
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("ligne 1");
      titleinesrslt.setText(IShowEventController.recup.getTitle());
      System.out.println("ligne 1");
        descinesrslt.setText(IShowEventController.recup.getDescription());
        System.out.println("ligne 1");
        etabinesrslt.setText(IShowEventController.recup.getEtablissement());
        System.out.println("ligne 1");
        locinesrslt.setText(IShowEventController.recup.getLocalisation());
        System.out.println("ligne 1");
        prixinesrslt.setText(Float.toString(IShowEventController.recup.getPrix()));
        System.out.println("ligne 1");
        dateinesrslt.setText(IShowEventController.recup.getDate().toLocalDate().toString());
        System.out.println("ligne 1");
        catinesrslt.setText(IShowEventController.recup.getCategory().toString());
        System.out.println("ligne 1");
      test.setText(IShowEventController.recup.getImagepath());
      System.out.println("ligne 1");
       path = test.getText();
       System.out.println("ligne 1");
       try {
            imageEvent.setImage(new Image(new FileInputStream(path)));
            System.out.println("ligne 1");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IOneShowEventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("ligne 1");
       
        imageEvent.setVisible(true);
        System.out.println("ligne 1");
        titleinesrslt.setText(IShowEventController.recup.getTitle() );
        descinesrslt.setText(IShowEventController.recup.getDescription());
        etabinesrslt.setText(IShowEventController.recup.getEtablissement());
        locinesrslt.setText(IShowEventController.recup.getLocalisation());
        prixinesrslt.setText(Float.toString(IShowEventController.recup.getPrix()));
        dateinesrslt.setText(IShowEventController.recup.getDate().toLocalDate().toString());
        catinesrslt.setText(IShowEventController.recup.getCategory().toString());
      
    }    

    public JFXTextField getEtabinesrslt() {
        return etabinesrslt;
    }

    public void setEtabinesrslt(String etabinesrslt) {
        this.etabinesrslt.setText(etabinesrslt);
    }

    public JFXTextField getTitleinesrslt() {
        return titleinesrslt;
    }

    public void setTitleinesrslt(String titleinesrslt) {
        this.titleinesrslt.setText(titleinesrslt);
    }

    public JFXTextField getDateinesrslt() {
        return dateinesrslt;
    }

    public void setDateinesrslt(String dateinesrslt) {
        this.dateinesrslt.setText(dateinesrslt);
    }

    public JFXTextField getPrixinesrslt() {
        return prixinesrslt;
    }

    public void setPrixinesrslt(String prixinesrslt) {
        this.prixinesrslt.setText(prixinesrslt);
    }

    public JFXTextField getLocinesrslt() {
        return locinesrslt;
    }

    public void setLocinesrslt(String locinesrslt) {
        this.locinesrslt.setText(locinesrslt);
    }

    public JFXTextArea getDescinesrslt() {
        return descinesrslt;
    }

    public void setDescinesrslt(String descinesrslt) {
        this.descinesrslt.setText(descinesrslt);
    }

    public JFXTextField getCatinesrslt() {
        return catinesrslt;
    }

    public void setCatinesrslt(String catinesrslt) {
        this.catinesrslt.setText(catinesrslt);
    }

    public JFXButton getGoparticipatebtn() {
        return goparticipatebtn;
    }

    public void setGoparticipatebtn(JFXButton goparticipatebtn) {
        this.goparticipatebtn = goparticipatebtn;
    }

    public JFXButton getReturnshow() {
        return returnshow;
    }

    public void setReturnshow(JFXButton returnshow) {
        this.returnshow = returnshow;
    }
    

    @FXML
    private void partEvent(ActionEvent event) {
          try {
           /* FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IParticipate.fxml"));
            Parent root= loader.load();
            IParticipateController rc= loader.getController();
            
            goparticipatebtn.getScene().setRoot(root);  */
             //  if(IOneShowEventController.usertest==1){
                 setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IParticipate.fxml"))));
            //}
              
            
        
              } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        } 
         
        
        
         }
    

    @FXML
    private void returnhomepage(ActionEvent event) {
        try {
         /*  FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));

            Parent root= loader.load();
            IShowEventController rc= loader.getController();
            
            returnshow.getScene().setRoot(root);   */
             
            if(IShowUserEventController.usertest==1){
                 setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowUserEvent.fxml"))));
            }else{
            
            
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    
    private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
 

    
}
