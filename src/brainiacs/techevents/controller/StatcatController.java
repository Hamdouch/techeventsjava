/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.IShowEventController;
import com.jfoenix.controls.JFXButton;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.connexion.MyConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class StatcatController implements Initializable {
    
    @FXML
    private AnchorPane panefornode;

    @FXML
    private PieChart pieEvent;
    int a=0;
    int b=0;
    int c=0;
    @FXML
    private JFXButton retourpie;
    @FXML
    private Label nbe;
    @FXML
    private Label nbcp;
    @FXML
    private Label nbcf;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
              String requete="select * FROM event where category=? ";
        
        ArrayList<Evenement> myEventList = new ArrayList();
        try {
            MyConnection myCNX=MyConnection.getMyConnection();
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,"Evénement");
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
               a++;
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        String requete1="select * FROM event where category=? ";
        
        ArrayList<Evenement> myEventList1 = new ArrayList();
        try {
            MyConnection myCNX=MyConnection.getMyConnection();

            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,"Compétition");
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
               b++;
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        String requete2="select * FROM event where category=? ";
        
        ArrayList<Evenement> myEventList2 = new ArrayList();
        try {
             MyConnection myCNX=MyConnection.getMyConnection();

            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,"Conférence");
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
               c++;
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        ObservableList<PieChart.Data> pieChartData =FXCollections.observableArrayList();
        pieChartData.addAll(new PieChart.Data("Conférences",c),
        new PieChart.Data("Evénements",a),
        new PieChart.Data("Compétitions",b)
        );
        pieEvent.setData(pieChartData);
        pieEvent.setLabelsVisible(true);
        nbe.setText(String.valueOf(a));
        nbcf.setText(String.valueOf(c));
        nbcp.setText(String.valueOf(b));
    }

    @FXML
    private void backToHome(ActionEvent event) {
         try {
      /*      FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));

            Parent root= loader.load();
            IShowEventController rc= loader.getController();
            
            retourpie.getScene().setRoot(root);   */
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
     private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
    
    
    
    }    
    

