/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.github.sarxos.webcam.Webcam;
import com.jfoenix.controls.JFXTextField;
import static brainiacs.techevents.controller.AdministrationController.ca;
import static brainiacs.techevents.controller.AdministrationController.dn;
import static brainiacs.techevents.controller.AdministrationController.lo;
import static brainiacs.techevents.controller.AdministrationController.m;
import static brainiacs.techevents.controller.AdministrationController.mp;
import static brainiacs.techevents.controller.AdministrationController.n;
import static brainiacs.techevents.controller.AdministrationController.nu;
import static brainiacs.techevents.controller.AdministrationController.nus;
import static brainiacs.techevents.controller.AdministrationController.pht;
import static brainiacs.techevents.controller.AdministrationController.pn;
import static brainiacs.techevents.controller.AdministrationController.userrecup;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 *
 * @author User
 */
public class ProfilController implements Initializable {
     @FXML
    private AnchorPane anshorpannnefornode;

      @FXML
    private JFXTextField Nom;
    @FXML
    private JFXTextField Prenom;
    @FXML
    private JFXTextField NomUtlisateur;
    @FXML
    private JFXTextField mail;
    @FXML
    private JFXTextField pwd;
    @FXML
    private JFXTextField datenaissance;
    @FXML
    private JFXTextField localisation;
    @FXML
    private JFXTextField num;
    @FXML
    private JFXTextField cat;
    @FXML
    private Label Nuser;
    @FXML
    private ImageView view;
    List<String> lstFile;
    @FXML
    private Label photo;
    

    /**
     * Initializes the controller class.
     */
    

    public void initialize(URL url, ResourceBundle rb) {

        Nuser.setText(userrecup);
        Nom.setText(n);
        Prenom.setText(pn);
        NomUtlisateur.setText(nus);
        mail.setText(m);
        pwd.setText(mp);
        datenaissance.setText(dn);
        localisation.setText(lo);
        photo.setText(AdministrationController.pht);
        try {
            view.setImage(new Image(new FileInputStream(pht)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(brainiacs.techevents.controller.ProfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
        num.setText(nu);
        cat.setText(ca);
        lstFile = new ArrayList<>();
        lstFile.add("*.png");
        lstFile.add("*.jpg");
       
        
        
        

    }

    @FXML
    private void modifierP(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Modification");
        alert.setHeaderText(null);
        alert.setContentText("voulez vous vraiment modifier cet Utilisateur ? ");
        Optional<ButtonType> action = alert.showAndWait();
        if (action.get() == ButtonType.OK) {
            Utilisateur u = new Utilisateur();
            u.setNom(Nom.getText());
            u.setPrenom(Prenom.getText());
            u.setUsername(NomUtlisateur.getText());
            u.setMail(mail.getText());
            u.setMot_de_passe(pwd.getText());
            u.setLocalisation(localisation.getText());
            u.setNum_tel(num.getText());
            u.setCategories(cat.getText());
            u.setDate_naissance(datenaissance.getText());
            UserCrud uc = new UserCrud();

            uc.modifierUtilisateur(AdministrationController.idrec, u.getNom(), u.getPrenom(), u.getUsername(), u.getMail(), u.getMot_de_passe()); // insertion dans la base de données
           uc.modifierPic(NomUtlisateur.getText(), photo.getText());
            try {

             /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"));
                Parent root = loader.load();
                AdministrationController rc = loader.getController();
                Nom.getScene().setRoot(root);  */
                 setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"))));

            } catch (IOException ex) {
                System.out.println(ex.getMessage());

            }
        }
    }
    
    
    private void keyphoto(InputMethodEvent event) {
        photo.setVisible(false);
    }
    @FXML
    private void capture(ActionEvent event) throws IOException {
        //voila
       /* Thread t = new Thread(){
            @Override
            public void run(){
                wCamPanel.start();
            }
        };
        t.setDaemon(true);
        t.start();*/
        //voila
        Webcam webcam = Webcam.getDefault();
        webcam.open();
        ImageIO.write(webcam.getImage(), "JPG", new File("First.jpg"));
    }

    @FXML
    private void importer(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("word file", lstFile));
        File f = fc.showOpenDialog(null);

        if (f != null) {
            try {
                photo.setText(f.getAbsolutePath());
                javafx.scene.image.Image image = new javafx.scene.image.Image(f.toURI().toURL().toString());
                view.setImage(image);
                view.yProperty();
               // photo.setVisible(false);
            } catch (MalformedURLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    
     private void setNode(Node node) {
        anshorpannnefornode.getChildren().clear();
        anshorpannnefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
    
    
    
}
