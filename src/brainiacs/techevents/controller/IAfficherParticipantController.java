/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.crud.ParticipantFormationCRUD;
import brainiacs.techevents.entities.ParticipantFormation;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author linda
 */
public class IAfficherParticipantController implements Initializable {
     @FXML
    private AnchorPane paneiafficheparticipant;
        @FXML
    private JFXButton btnreetouraafiih;
    @FXML
    private TableColumn<ParticipantFormation, String> cCin;
    @FXML
    private TableColumn<ParticipantFormation, String> cNom;
    @FXML
    private TableColumn<ParticipantFormation, String> cPrenom;
    @FXML
    private TableColumn<ParticipantFormation, String> cMail;
    @FXML
    private TableColumn<ParticipantFormation, String> cAdresse;
    @FXML
    private TableColumn<ParticipantFormation, String> cTelephone;
    @FXML
    private TableView<ParticipantFormation> tabPart;
    public static ParticipantFormation participantRecup;
  
    @FXML
    private TextField ftRecherche;
    
      ObservableList<ParticipantFormation> afficherParticipant;
  

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cCin.setCellValueFactory(new PropertyValueFactory<>("CIN"));
        cNom.setCellValueFactory(new PropertyValueFactory<>("Nom"));
        cPrenom.setCellValueFactory(new PropertyValueFactory<>("Prenom"));
        cMail.setCellValueFactory(new PropertyValueFactory<>("Mail"));
        cAdresse.setCellValueFactory(new PropertyValueFactory<>("Adresse"));
        cTelephone.setCellValueFactory(new PropertyValueFactory<>("NumTelephone"));
        
      //  addButtonUpdateToTable();
        ///addButtonDeleteToTable();
        
           ParticipantFormation p = new ParticipantFormation();
        ParticipantFormationCRUD pfc = new ParticipantFormationCRUD();
        
        afficherParticipant = FXCollections.observableArrayList(pfc.afficherParticipant());
        tabPart.setItems(afficherParticipant);
        
    }
 /*   private void addButtonUpdateToTable() {
        TableColumn<ParticipantFormation, Void> colBtn = new TableColumn("Modifier Participant");
        
      Callback<TableColumn<ParticipantFormation, Void>, TableCell<ParticipantFormation, Void>> cellFactory = new Callback<TableColumn<ParticipantFormation, Void>, TableCell<ParticipantFormation, Void>>() {
          
            public TableCell<ParticipantFormation, Void> call(final TableColumn<ParticipantFormation, Void> param) {
                final TableCell<ParticipantFormation, Void> cell = new TableCell<ParticipantFormation, Void>() {

                    private final Button btn = new Button("Modifier");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            participantRecup=getTableView().getItems().get(getIndex());
                            
                                 try {

                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IModifierParticipant.fxml"));
                                Parent root = loader.load();
                                IModifierParticipantController rc = loader.getController();
                               btn.getScene().setRoot(root);

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabPart.getColumns().add(colBtn);

    }
    
    private void addButtonDeleteToTable() {
        TableColumn<ParticipantFormation, Void> colBtn = new TableColumn("Supprimer Participant");

        Callback<TableColumn<ParticipantFormation, Void>, TableCell<ParticipantFormation, Void>> cellFactory = new Callback<TableColumn<ParticipantFormation, Void>, TableCell<ParticipantFormation, Void>>() {
           
            @Override
            public TableCell<ParticipantFormation, Void> call(final TableColumn<ParticipantFormation, Void> param) {
                final TableCell<ParticipantFormation, Void> cell = new TableCell<ParticipantFormation, Void>() {

                    private final Button btn = new Button("Supprimer");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            ParticipantFormation p = getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Suppression");
                            alert.setHeaderText(null);
                            alert.setContentText("Voulez vous vraiment supprimer cet participant ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                ParticipantFormationCRUD pfc = new ParticipantFormationCRUD();
                                pfc.supprimerParticipant(p.getCIN()); 

                            }

                            try {

                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherParticipant.fxml"));
                                Parent root = loader.load();
                                IAfficherParticipantController rc = loader.getController();
                               btn.getScene().setRoot(root);

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabPart.getColumns().add(colBtn);

    }  */
    @FXML
           private void chercher(KeyEvent event) {
    
        cCin.setCellValueFactory(new PropertyValueFactory<>("CIN"));
        cNom.setCellValueFactory(new PropertyValueFactory<>("Nom"));
        cPrenom.setCellValueFactory(new PropertyValueFactory<>("Prenom"));
        cMail.setCellValueFactory(new PropertyValueFactory<>("Mail"));
        cAdresse.setCellValueFactory(new PropertyValueFactory<>("Adresse"));
        cTelephone.setCellValueFactory(new PropertyValueFactory<>("NumTelephone"));
        
        ParticipantFormationCRUD pfc =new ParticipantFormationCRUD();
        
        afficherParticipant=FXCollections.observableArrayList(pfc.chercherPrenomParticipant(ftRecherche.getText()));
        tabPart.setItems(afficherParticipant);
    } 
           
             @FXML
    void retourIafficheForm(ActionEvent event) {
         try {
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));
         } catch (IOException ex) {
             Logger.getLogger(IAfficherParticipantController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
      private void setNode(Node node) {
        
        paneiafficheparticipant.getChildren().clear();
       paneiafficheparticipant.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }

}
    

