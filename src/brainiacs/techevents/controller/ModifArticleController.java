/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.ArticleCrud;
import brainiacs.techevents.entities.Article;
import static brainiacs.techevents.controller.GestionDesArticlesController.articleRecup;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import javax.swing.JFileChooser;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ModifArticleController implements Initializable {
    @FXML
    private AnchorPane pannefornodeeh;

    @FXML
    private Label lbNotifUpdate;
    @FXML
    private JFXTextField ftUpAuteur;
    @FXML
    private JFXTextField ftUpJournal;
    @FXML
    private JFXTextField ftUpTitre;
    @FXML
    private JFXTextField ftUpPath;
    @FXML
    private JFXButton btValUpArticle;
    @FXML
    private JFXButton btRetourModif;
    @FXML
    private Label lbvide;

    public JFXTextField getFtUpAuteur() {
        return ftUpAuteur;
    }

    public JFXTextField getFtUpJournal() {
        return ftUpJournal;
    }

    public JFXTextField getFtUpTitre() {
        return ftUpTitre;
    }

    public JFXTextField getFtUpPath() {
        return ftUpPath;
    }

    public void setLbNotifUpdate(Label lbNotifUpdate) {
        this.lbNotifUpdate = lbNotifUpdate;
    }

    public void setFtUpAuteur(String ftUpAuteur) {
        this.ftUpAuteur.setText(ftUpAuteur);
    }

    public void setFtUpJournal(String ftUpJournal) {
        this.ftUpJournal.setText(ftUpJournal);
    }

    public void setFtUpTitre(String ftUpTitre) {
        this.ftUpTitre.setText(ftUpTitre);
    }

    public void setFtUpPath(String ftUpPath) {
        this.ftUpPath.setText(ftUpPath);
    }
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ftUpAuteur.setText(GestionDesArticlesController.articleRecup.getAuteur());
        ftUpJournal.setText(GestionDesArticlesController.articleRecup.getJournal());
        ftUpPath.setText(GestionDesArticlesController.articleRecup.getEmplacement());
        ftUpTitre.setText(GestionDesArticlesController.articleRecup.getTitre());
    }    

    @FXML
    private void validerModifArticle(ActionEvent event) {
        
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Modification");
                            alert.setHeaderText(null);
                            alert.setContentText("voulez vous vraiment modifier cet article ? ");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK)
                            {
                                ArticleCrud ac=new ArticleCrud();
        
      
        ac.modifierArticle(GestionDesArticlesController.articleRecup.getIdentifiant(), ftUpAuteur.getText(), ftUpJournal.getText(), ftUpTitre.getText(), ftUpPath.getText()); // insertion dans la base de données
          try {

                              /*  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"));
                                Parent root = loader.load();
                               GestionDesArticlesController rc = loader.getController();
                               ftUpAuteur.getScene().setRoot(root);   */
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
         }
        
        
        
        
       
    }

    @FXML
    private void retourModifArtVersGestionArt(ActionEvent event) {
          try {
         /*   FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"));
            Parent root= loader.load();
            GestionDesArticlesController rc= loader.getController();  
               btRetourModif.getScene().setRoot(root);*/
              setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"))));
            
           
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
        
    }

    @FXML
    private void insertFile(ActionEvent event) {
        JFileChooser chooser=new JFileChooser();
chooser.showOpenDialog(null);
File f=chooser.getSelectedFile();
String filename=f.getAbsolutePath();
ftUpPath.setText(filename);
    }
    
    
    private void setNode(Node node) {
        pannefornodeeh.getChildren().clear();
        pannefornodeeh.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
}
