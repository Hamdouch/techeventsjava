/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.FormationCRUD;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Optional;

import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;


/**
 * FXML Controller class
 *
 * @author linda
 */
public class IModifierFormationController implements Initializable {
       @FXML
    private AnchorPane panefornode;

    @FXML
    private JFXTextField fttitre;
    @FXML
    private JFXTextField ftetab;
    @FXML
    private JFXTextArea ftdesc;
    @FXML
    private DatePicker ftdatedebut;
    @FXML
    private DatePicker ftdatefin;
    @FXML
    private JFXTextField ftprix;
    @FXML
    private JFXButton btnvalider;
    @FXML
    private JFXButton btRtAJAff;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        ftdesc.setText(IAfficherFormationController.formationRecup.getDescription());
        fttitre.setText(IAfficherFormationController.formationRecup.getTitre());
        ftetab.setText(IAfficherFormationController.formationRecup.getEtablissement());
        ftprix.setText(Float.toString(IAfficherFormationController.formationRecup.getPrix()));
        ftdatedebut.setValue(IAfficherFormationController.formationRecup.getDate_debut().toLocalDate());
        ftdatefin.setValue(IAfficherFormationController.formationRecup.getDate_fin().toLocalDate());
    }

    @FXML
    private void modifierFormation(ActionEvent event) {
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Modification");
                            alert.setHeaderText(null);
                            alert.setContentText("voulez vous vraiment modifier cette formation ? ");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK)
                            {
                                FormationCRUD fc=new FormationCRUD();
                                fc.modifierFormation(IAfficherFormationController.formationRecup.getId(),fttitre.getText() , ftdesc.getText(), ftetab.getText(),Date.valueOf(ftdatedebut.getValue()),Date.valueOf(ftdatefin.getValue()), Float.parseFloat(ftprix.getText()));
                                
                                   try {

                               /* FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
                                Parent root = loader.load();
                               IAfficherFormationController rc = loader.getController();
                               ftprix.getScene().setRoot(root);   */
                                        setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                                                    }
                             }
    }
   

    @FXML
    private void retournerAjoutVersAffichage(ActionEvent event) {
        
        try {
         /*   FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
            Parent root= loader.load();
            IAfficherFormationController rc= loader.getController();
            
            btRtAJAff.getScene().setRoot(root);   */
            
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    
      private void setNode(Node node) {
         
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
}
