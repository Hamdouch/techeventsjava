/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.github.sarxos.webcam.Webcam;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import static brainiacs.techevents.controller.AdministrationController.ut;

import brainiacs.techevents.controller.ProfilUserController;
import static brainiacs.techevents.controller.LoginController.ute;
import static brainiacs.techevents.controller.ProfilUserController.cae;
import static brainiacs.techevents.controller.ProfilUserController.dne;
import static brainiacs.techevents.controller.ProfilUserController.loe;
import static brainiacs.techevents.controller.ProfilUserController.me;
import static brainiacs.techevents.controller.ProfilUserController.mpe;
import static brainiacs.techevents.controller.ProfilUserController.ne;
import static brainiacs.techevents.controller.ProfilUserController.nue;
import static brainiacs.techevents.controller.ProfilUserController.nuse;
import static brainiacs.techevents.controller.ProfilUserController.phte;
import static brainiacs.techevents.controller.ProfilUserController.pne;
import static brainiacs.techevents.controller.ProfilUserController.userrecupe;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author User
 */
public class ModifProUserController implements Initializable {
      @FXML
    private AnchorPane modifprouserpane;
    @FXML
    private JFXTextField Nom;
    @FXML
    private JFXTextField Prenom;
    @FXML
    private JFXTextField NomUSer;
    @FXML
    private JFXTextField Mail;
    @FXML
    private JFXTextField pwd;
    @FXML
    private JFXTextField DateN;
    @FXML
    private JFXTextField loc;
    @FXML
    private JFXTextField gsm;
    @FXML
    private ImageView pic;
    @FXML
    private JFXTextField cate;
    @FXML
    private Label pathe;
     @FXML
    private Label NUser;
     List<String> lstFile;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NUser.setText(userrecupe);
        Nom.setText(ne);
        Prenom.setText(pne);
        NomUSer.setText(nuse);
        Mail.setText(me);
        pwd.setText(mpe);
        DateN.setText(dne);
        loc.setText(loe);
        pathe.setText(ProfilUserController.phte);
        try {
            pic.setImage(new Image(new FileInputStream(phte)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(brainiacs.techevents.controller.ModifProUserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        gsm.setText(nue);
        cate.setText(cae);
        lstFile = new ArrayList<>();
        lstFile.add("*.png");
        lstFile.add("*.jpg");
    }    

    @FXML
    private void importPhuser(ActionEvent event) {
         FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("word file", lstFile));
        File f = fc.showOpenDialog(null);

        if (f != null) {
            try {
                pathe.setText(f.getAbsolutePath());
                ute.setPhoto(pathe.getText());
                UserCrud uce = new UserCrud();
                uce.modifierPic(ute.getUsername(), ute.getPhoto());
                javafx.scene.image.Image image = new javafx.scene.image.Image(f.toURI().toURL().toString());
                pic.setImage(image);
                pic.yProperty();
               // photo.setVisible(false);
            } catch (MalformedURLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @FXML
    private void capUser(ActionEvent event) throws IOException {
        Webcam webcam = Webcam.getDefault();
        webcam.open();
        File Re = new File("First.jpg");
        ImageIO.write(webcam.getImage(), "JPG", new File("First.jpg"));
        
        //webcam.wait(timeout);
        webcam.close();
        ////////////////////////////////////////
        pathe.setText(Re.getAbsolutePath());
        ute.setPhoto(pathe.getText());
                UserCrud uce = new UserCrud();
                uce.modifierPic(ute.getUsername(), ute.getPhoto());
                javafx.scene.image.Image image = new javafx.scene.image.Image(Re.toURI().toURL().toString());
                pic.setImage(image);
                pic.yProperty();
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            
            if(ProfilEtabController.direction==0){
                  setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilEtab.fxml"))));
            }else{
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"))));
            }
          //  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"));
           // Parent root = loader.load();
           // ProfilUserController rc = loader.getController();

           // cate.getScene().setRoot(root);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void enregModif(ActionEvent event) {
         Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Modification");
        alert.setHeaderText(null);
        alert.setContentText("voulez vous vraiment modifier Votre profil ? ");
        Optional<ButtonType> action = alert.showAndWait();
         if (action.get() == ButtonType.OK) {
            Utilisateur u = new Utilisateur();
            u.setNom(Nom.getText());
            u.setPrenom(Prenom.getText());
            u.setUsername(NomUSer.getText());
            u.setMail(Mail.getText());
            u.setMot_de_passe(pwd.getText());
            u.setLocalisation(loc.getText());
            u.setNum_tel(gsm.getText());
            u.setCategories(cate.getText());
            u.setDate_naissance(DateN.getText());
            u.setPhoto(pathe.getText());
            UserCrud uc = new UserCrud();
            

            uc.modifierUtilisateur(ProfilUserController.idrece, u.getNom(), u.getPrenom(), u.getUsername(), u.getMail(), u.getMot_de_passe()); // insertion dans la base de données
           uc.modifierPic(NUser.getText(), pathe.getText());
    
         
         try {
            
            if(ProfilEtabController.direction==0){
                  setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilEtab"))));
            }else{
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"))));
            }
          //  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"));
           // Parent root = loader.load();
           // ProfilUserController rc = loader.getController();

           // cate.getScene().setRoot(root);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
         
         }
       /*   try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/login.fxml"));
            Parent root = loader.load();
            ProfilController rc = loader.getController();

            cate.getScene().setRoot(root);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
              */
         
    
}
    
      private void setNode(Node node) {
       modifprouserpane.getChildren().clear();
     modifprouserpane.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
    
    
    
    
}
