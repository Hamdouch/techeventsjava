/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Asus     
 */
public class EventFXMain extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try {
            
         // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAddEvent.fxml"));
          // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IParticipate.fxml"));
             //Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IPay.fxml"));
            
   Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));
        // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizTech.fxml"));
       // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/team.fxml"));
        // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/statcat.fxml"));
       // Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowUserEvent.fxml"));
            
         //   Parent root = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"));
         

            Scene scene = new Scene(root);
            
           // primaryStage.setTitle("Hello please add an event");
            primaryStage.setTitle("Please insert your information");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
       
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
