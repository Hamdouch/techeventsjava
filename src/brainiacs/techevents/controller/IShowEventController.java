/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.IOneShowEventController;
import brainiacs.techevents.controller.IModifyEventController;
import brainiacs.techevents.controller.IAddEventController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.EventCrud;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.entities.Type;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IShowEventController implements Initializable {
    @FXML
    private AnchorPane panefornode;

    @FXML
    private TableView<Evenement> tabEvent;
    @FXML
    private TableColumn<Evenement, String> columnEtab;
    @FXML
    private TableColumn<Evenement, String> columnTitre;
    @FXML
    private TableColumn<Evenement, Type> columnCat;
    @FXML
    private TableColumn<Evenement, String> columnDesc;
    @FXML
    private TableColumn<Evenement, Date> columnDate;
    @FXML
    private TableColumn<Evenement, String> columnLoc;
    @FXML
    private TableColumn<Evenement, Float> columnPrix;
    private TableColumn<Evenement, String> columnAfficher;
    
    
    ObservableList<Evenement> showEvent;
    static Evenement recup;
    @FXML
    private JFXTextField inesSearch;
    public static String trecup="";
    public static float prixrecup=0;
    @FXML
    private TableColumn<Evenement,Integer> nmbrp;
    @FXML
    private JFXButton btnajout;
    @FXML
    private JFXButton statsee;
    @FXML
    private JFXButton quizbtn;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        columnEtab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        columnTitre.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
       columnCat.setCellValueFactory(new PropertyValueFactory<>("category"));
        columnLoc.setCellValueFactory(new PropertyValueFactory<>("localisation"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        nmbrp.setCellValueFactory(new PropertyValueFactory<>("nombreParticipants"));
       
        
       
       
       
       addDeleteBtn();
       addBtnUpdate();
       showThisEvent();
        
        Evenement e = new Evenement ();
        EventCrud ec = new EventCrud();
        
        showEvent = FXCollections.observableArrayList(ec.afficherEvent());
        tabEvent.setItems(showEvent);
        
        
    }    
   
    

    private void addDeleteBtn() {
TableColumn<Evenement, Void> colBtn = new TableColumn("Suppression");

        Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>> cellFactory = new Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>>() {
            @Override
            public TableCell<Evenement, Void> call(final TableColumn<Evenement, Void> param) {
                final TableCell<Evenement, Void> cell = new TableCell<Evenement, Void>() {

                    private final JFXButton btn = new JFXButton("Supprimer");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Evenement e= getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Supprimer l'événement");
                            alert.setHeaderText(null);
                            alert.setContentText("Vous voulez vraiment supprimer l'événement "  + e.getTitle() + " ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                EventCrud ec = new EventCrud();
                                ec.supprimerEvent(e.getId()); 

                            }

                            try {

                          /*      FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));
                                Parent root = loader.load();
                                IShowEventController rc = loader.getController();
                               tabEvent.getScene().setRoot(root);   */
                              
setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));  
                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabEvent.getColumns().add(colBtn);
    }
    private void addBtnUpdate() {
        TableColumn<Evenement, Void> colBtn = new TableColumn("Modification");

        Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>> cellFactory = new Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>>() {
            @Override
            public TableCell<Evenement, Void> call(final TableColumn<Evenement, Void> param) {
                final TableCell<Evenement, Void> cell = new TableCell<Evenement, Void>() {

                    private final JFXButton btn = new JFXButton("Modifier");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            recup=getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Modification");
                            alert.setHeaderText(null);
                            alert.setContentText("Voulez vous vraiment modifier cet événement ?");
                            Optional<ButtonType> action= alert.showAndWait();
                           
                                 try {
                          /*      FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IModifyEvent.fxml"));
                                Parent root = loader.load();
                               IModifyEventController rc = loader.getController();
                               btn.getScene().setRoot(root);   */
                             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IModifyEvent.fxml"))));        

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabEvent.getColumns().add(colBtn);

    }


    private void showThisEvent(){
        TableColumn<Evenement, Void> colBtn = new TableColumn("Afficher");

        Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>> cellFactory = new Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>>() {
            @Override
            public TableCell<Evenement, Void> call(final TableColumn<Evenement, Void> param) {
                final TableCell<Evenement, Void> cell = new TableCell<Evenement, Void>() {

                    private final JFXButton btn = new JFXButton("Afficher la suite");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            recup=getTableView().getItems().get(getIndex());
                            trecup=recup.getTitle();
                            prixrecup=recup.getPrix();
            
                                 try {
                          /*      FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"));
                                Parent root = loader.load();
                               IOneShowEventController rc = loader.getController();
                               btn.getScene().setRoot(root);   */
                                      setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

       tabEvent.getColumns().add(colBtn);

    }

    @FXML
    private void searchEvent(KeyEvent event) {
         columnEtab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        columnTitre.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
       columnCat.setCellValueFactory(new PropertyValueFactory<>("category"));
        columnLoc.setCellValueFactory(new PropertyValueFactory<>("localisation"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        
    //    Evenement e = new Evenement();
        EventCrud ec = new EventCrud();
        
        showEvent=FXCollections.observableArrayList(ec.rechercheTitreEvent(inesSearch.getText()));

        tabEvent.setItems(showEvent);
    }

    @FXML
    private void redirectAddEvent(ActionEvent event) {
        try {
              /*    FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAddEvent.fxml"));
                  Parent root = loader.load();
                  IAddEventController rc = loader.getController();
                  btnajout.getScene().setRoot(root); */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAddEvent.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
    }

    @FXML
    private void redirectStatCat(ActionEvent event) {
         try {
               /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/statcat.fxml"));
                  Parent root = loader.load();
                  StatcatController rc = loader.getController();
                  statsee.getScene().setRoot(root);   */
             
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/statcat.fxml"))));
                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
    }

    @FXML
    private void redirectQuiz(ActionEvent event) {
         try {
               /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"));
                  Parent root = loader.load();
                  QuizListController rc = loader.getController();
                  quizbtn.getScene().setRoot(root);   */
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"))));
             
                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
    }
    
     
    private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

    
    }
    

    

