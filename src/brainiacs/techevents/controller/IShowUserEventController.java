/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.IOneShowEventController;
import static brainiacs.techevents.controller.IShowEventController.prixrecup;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.EventCrud;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.entities.Type;
import static brainiacs.techevents.controller.IShowEventController.recup;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IShowUserEventController implements Initializable {
    @FXML
    private AnchorPane panefornode;

    @FXML
    private TableView<Evenement> tabEvent;
    @FXML
    private TableColumn<Evenement,String> columnEtab;
    @FXML
    private TableColumn<Evenement, String> columnTitre;
    @FXML
    private TableColumn<Evenement, Type> columnCat;
    @FXML
    private TableColumn<Evenement, String> columnDesc;
    @FXML
    private TableColumn<Evenement, Date> columnDate;
    @FXML
    private TableColumn<Evenement, String> columnLoc;
    @FXML
    private TableColumn<Evenement, Float> columnPrix;
    ObservableList<Evenement> showEvent;
    static Evenement recup;
    @FXML
    private JFXTextField inesSearch;
    public static String trecup="";
    public static int usertest;
    @FXML
    private TableColumn<Evenement,Integer> nmbrp;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         columnEtab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        columnTitre.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
       columnCat.setCellValueFactory(new PropertyValueFactory<>("category"));
        columnLoc.setCellValueFactory(new PropertyValueFactory<>("localisation"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        nmbrp.setCellValueFactory(new PropertyValueFactory<>("nombreParticipants"));
        
        showThisEvent();
        Evenement e = new Evenement ();
        EventCrud ec = new EventCrud();
        
        showEvent = FXCollections.observableArrayList(ec.afficherEvent());
        tabEvent.setItems(showEvent);
    }    

    @FXML
    private void searchEvent(KeyEvent event) {
        columnEtab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        columnTitre.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
       columnCat.setCellValueFactory(new PropertyValueFactory<>("category"));
        columnLoc.setCellValueFactory(new PropertyValueFactory<>("localisation"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        nmbrp.setCellValueFactory(new PropertyValueFactory<>("nombreParticipants"));

        
        Evenement e = new Evenement();
        EventCrud ec = new EventCrud();
        
        showEvent=FXCollections.observableArrayList(ec.rechercheTitreEvent(inesSearch.getText()));

        tabEvent.setItems(showEvent);
    }
    private void showThisEvent(){
        TableColumn<Evenement, Void> colBtn = new TableColumn("Afficher");

        Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>> cellFactory = new Callback<TableColumn<Evenement, Void>, TableCell<Evenement, Void>>() {
            @Override
            public TableCell<Evenement, Void> call(final TableColumn<Evenement, Void> param) {
                final TableCell<Evenement, Void> cell = new TableCell<Evenement, Void>() {

                    private final JFXButton btn = new JFXButton("Afficher la suite");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            recup=getTableView().getItems().get(getIndex());
                            trecup=recup.getTitle();
                                                        prixrecup=recup.getPrix();

            
                                 try {
                             /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"));
                                Parent root = loader.load();
                               IOneShowEventController rc = loader.getController();
                               btn.getScene().setRoot(root);   */
                                     
 setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IOneShowEvent.fxml"))));
                             usertest=1;//pour détecter user
                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

       tabEvent.getColumns().add(colBtn);

    }
   
    
    
    
    private void setNode(Node node) {
       panefornode.getChildren().clear();
       panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
    
}
