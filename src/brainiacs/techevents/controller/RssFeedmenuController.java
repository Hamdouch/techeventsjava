/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import static brainiacs.techevents.controller.GActualiteController.selectionnedAct;
import brainiacs.techevents.crud.GestionActualite;
import brainiacs.techevents.entities.Actualite;
import com.jfoenix.controls.JFXButton;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import static javafx.collections.FXCollections.observableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class RssFeedmenuController implements Initializable {
       @FXML
    private AnchorPane panerss;

    @FXML
    private TableView<Actualite> TableViewActualite;
    @FXML
    private TextField RssTextField;
    @FXML
    private JFXButton ajoutbtn;
     @FXML
    private ImageView back;
     private static ObservableList<Actualite> row;

  // ObservableList<Actualite> dataActualitelist;
    //ArrayList arrayList = null;
 static Actualite selectionnedAct;
    /**
     * Initializes the controller class.
     */
 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TableColumn<Actualite, String> titreColumn = new TableColumn<>("Titre");
        titreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));
        titreColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> descColumn = new TableColumn<>("Description");
        descColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        descColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> categColumn = new TableColumn<>("Categories");
        categColumn.setCellValueFactory(new PropertyValueFactory<>("categories"));
        categColumn.setStyle("-fx-alignment: CENTER;");
        
         ///
        TableColumn<Actualite, Date> dateColumn = new TableColumn<>("date");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> screenshotColumn = new TableColumn<>("PHOTO");
        screenshotColumn.setCellValueFactory(new PropertyValueFactory<>("pictures"));
        screenshotColumn.setStyle("-fx-alignment: CENTER;");
     // ajouter les collones et déclaration de list observable pour la data
        TableViewActualite.getColumns().addAll(dateColumn,screenshotColumn,titreColumn,categColumn, descColumn);
         row= FXCollections.observableArrayList();
         addButtonAjoutertoTable();

       //affichage de l'image dans table view 
        
        Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>> cellFactoryImage
                = //
                new Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>>() {
            @Override
            public TableCell call(final TableColumn<Actualite, String> param) {
                final TableCell<Actualite, String> cell = new TableCell<Actualite, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            try {
                                ImageView imagev = new ImageView(new Image(new FileInputStream(item)));
                                imagev.setFitHeight(120);
                                imagev.setFitWidth(200);
                                setGraphic(imagev);
                                setText(null);
                                //System.out.println(item);
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                };
                
                cell.setOnMouseClicked((MouseEvent event2)
                        -> {
                    if (event2.getClickCount() == 1) {
                        if (TableViewActualite.getSelectionModel().getSelectedItem() != null && !TableViewActualite.getSelectionModel().getSelectedItem().getPictures().contains("null")) {
                            Stage window = new Stage();
//
                            window.setMinWidth(250);
                            ImageView imagevPOPUP;
                            try {
                                imagevPOPUP = new ImageView(new Image(new FileInputStream( TableViewActualite.getSelectionModel().getSelectedItem().getPictures())));
                                imagevPOPUP.setFitHeight(576);
                            imagevPOPUP.setFitWidth(1024);
                            
                            VBox layout = new VBox(10);
                            layout.getChildren().addAll(imagevPOPUP);
                            layout.setAlignment(Pos.CENTER);

                            //Display window and wait for it to be closed before returning
                            Scene scene = new Scene(layout);
                            window.setScene(scene);
                            window.show();
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                           

                        }
                    }
                });
                
                
                return cell;
            }
        };

       screenshotColumn.setCellFactory(cellFactoryImage);
        
        
        
        
        
        
    }    

    
    
    
 
    
    
    @FXML
    private void listRecherche(KeyEvent event) {
    }
  
  
    @FXML
    private void Rssreaderajout(ActionEvent event) throws MalformedURLException, IllegalArgumentException {
            try {
                
                
               
               String urlrech =RssTextField.getText();
               RssTextField.clear();
               
            URL url = new URL(urlrech);
            HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(httpURLConnection));
            List entries = feed.getEntries();
            Iterator itEntries = entries.iterator();
            GestionActualite GA = new GestionActualite();
           
            
            while(itEntries.hasNext()){
               
                SyndEntry entry = (SyndEntry) itEntries.next();
                 Actualite act = new Actualite();
                act.setTitre(entry.getTitle());
                act.setDescription(entry.getDescription().getValue());
                act.setDate(entry.getPublishedDate());
                act.setPictures("C:\\Users\\ASUS\\Desktop\\actualites.jpg");
                act.setCategories("TechEvents");
                row.add(act);
                   
             
              
             
              /*  System.out.println("Title : "+ entry.getTitle());
                System.out.println("Link : "+ entry.getLink());
                System.out.println("Author : "+ entry.getAuthor());
                System.out.println("Publish Date : "+entry.getPublishedDate());   */
               // System.out.println("Description : " + ); 
            }
           TableViewActualite.setItems(row);
        } catch (MalformedURLException ex) {
                System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println( ex.getMessage());
        } catch (IllegalArgumentException ex) {
             System.out.println(ex.getMessage());
        } catch (FeedException ex) {
             System.out.println(ex.getMessage());
        }
       


        
        
        
    }
    
    
  private void addButtonAjoutertoTable() {
        TableColumn<Actualite, Void> colBtn = new TableColumn("Ajouter Actualite");

        Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>> cellFactory = new Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>>() {
            @Override
            public TableCell<Actualite, Void> call(final TableColumn<Actualite, Void> param) {
                final TableCell<Actualite, Void> cell = new TableCell<Actualite, Void>() {

                    private final JFXButton btn = new JFXButton("Ajouter");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Actualite act = getTableView().getItems().get(getIndex());
                             java.util.Date dateutil = new java.util.Date();
                             java.util.Date datesql = new java.sql.Date(dateutil.getTime());
                             act.setDate(datesql);
                             act.setUser("TechEvents");
                           
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Ajouter a L'actualité ");
                            alert.setHeaderText(null);
                            alert.setContentText("Vous voulez vraiment Ajouter l'actualite de Titre " + act.getTitre() + " ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("Ajout d'actualite de titre " + act.getTitre() + " avec succès")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(3));
                                n.showInformation();
                                GestionActualite GA = new GestionActualite();
                                GA.ajouterActualite(act); //supprimer T3amlet

                            } else {
                                Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("Ajout d'actualite annuller")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(3));
                                n.showInformation();
                            }

                            try {

                                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/RssFeedmenu.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        TableViewActualite.getColumns().add(colBtn);

    }
    
     @FXML
    void backToActualite(MouseEvent event) {
           try {
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));
           } catch (IOException ex) {
               System.out.println(ex.getMessage());
           }
    }
 private void setNode(Node node) {
        panerss.getChildren().clear();
        panerss.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
}
