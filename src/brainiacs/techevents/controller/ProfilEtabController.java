/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import static brainiacs.techevents.controller.LoginController.ph;
import static brainiacs.techevents.controller.LoginController.username;
import static brainiacs.techevents.controller.LoginController.ute;
import static brainiacs.techevents.controller.ProfilUserController.cae;
import static brainiacs.techevents.controller.ProfilUserController.dne;
import static brainiacs.techevents.controller.ProfilUserController.idrece;
import static brainiacs.techevents.controller.ProfilUserController.loe;
import static brainiacs.techevents.controller.ProfilUserController.me;
import static brainiacs.techevents.controller.ProfilUserController.mpe;
import static brainiacs.techevents.controller.ProfilUserController.ne;
import static brainiacs.techevents.controller.ProfilUserController.nue;
import static brainiacs.techevents.controller.ProfilUserController.nuse;
import static brainiacs.techevents.controller.ProfilUserController.phte;
import static brainiacs.techevents.controller.ProfilUserController.pne;
import static brainiacs.techevents.controller.ProfilUserController.userrecupe;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author User
 */
public class ProfilEtabController implements Initializable {
    @FXML
    private AnchorPane profiletabnode;

    @FXML
    private Label labEvente;
    @FXML
    private Label labArt;
    @FXML
    private Label LabFor;
      @FXML
    private Label NomEtab;
          @FXML
    private Label pathImgEtab;
          @FXML
    private ImageView imageProfEtab;
        public static int direction;  
      List<String> lstFile;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NomEtab.setText(username);
        pathImgEtab.setText(LoginController.ph);
       try {
            imageProfEtab.setImage(new Image(new FileInputStream(ph)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(brainiacs.techevents.controller.ProfilUserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        lstFile = new ArrayList<>();
        lstFile.add("*.png");
        lstFile.add("*.jpg");
    }    

    @FXML
    private void ajoutpicEtab(ActionEvent event) {
         FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("word file", lstFile));
        File f = fc.showOpenDialog(null);

        if (f != null) {
            try {
                pathImgEtab.setText(f.getAbsolutePath());
                javafx.scene.image.Image image = new javafx.scene.image.Image(f.toURI().toURL().toString());
                imageProfEtab.setImage(image);
                imageProfEtab.yProperty();
                ute.setPhoto(pathImgEtab.getText());
                UserCrud uc = new UserCrud();
                uc.modifierPic(NomEtab.getText(), pathImgEtab.getText());
                // photo.setVisible(false);
            } catch (MalformedURLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @FXML
    private void DesactiverEtab(ActionEvent event) throws IOException {
          Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Desactivation");
        alert.setHeaderText(null);
        alert.setContentText("Vous voulez vraiment desactiver votre compte ! c'est irréversible !");

        Connection connection;
        PreparedStatement ps;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/techevents", "root", "");

            ps = connection.prepareStatement("SELECT * FROM `membres` WHERE `username` = ?");
            ps.setString(1, String.valueOf(NomEtab.getText()));
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                Utilisateur ut = new Utilisateur(result.getInt("id"), result.getString("nom"), result.getString("prenom"), result.getString("username"), result.getString("mail"), result.getString("mot_de_passe"), result.getString("date_naissance"), result.getString("localisation"), result.getString("num_tel"), result.getString("categories"), result.getString("photo"));
                Optional<ButtonType> action = alert.showAndWait();

                if (action.get() == ButtonType.OK) {
                    UserCrud ac = new UserCrud();
                    ac.supprimerUtilisateur(ut.getId()); //supprimer T3amlet

                }
            }

            /////////////////////////////////////////////
          /*  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/login.fxml"));
            Parent root1 = loader.load(); //chargement du show root

            //  LoginController rc = loader.getController();
            LabFor.getScene().setRoot(root1);

            // rc.lbNotifAjout.setText("Article ajouté avec succès!");   */
                 ////////////////////////////////////////////
            
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/login.fxml"))));
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void DeconnecterEtab(ActionEvent event) {
       /* try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/login.fxml"));
            Parent root1 = loader.load(); //chargement du show root

            //  LoginController rc = loader.getController();
            labArt.getScene().setRoot(root1);

            // rc.lbNotifAjout.setText("Article ajouté avec succès!");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }  */
    }

    @FXML
    private void modifProfEtab(ActionEvent event) throws SQLException {
         Connection connection;
        PreparedStatement ps;
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/techevents", "root", "");
        ps = connection.prepareStatement("SELECT * FROM `membres` WHERE `username` = ?");
        ps.setString(1, String.valueOf(NomEtab.getText()));
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            Utilisateur ut = new Utilisateur(result.getInt("id"), result.getString("nom"), result.getString("prenom"), result.getString("username"), result.getString("mail"), result.getString("mot_de_passe"), result.getString("date_naissance"), result.getString("localisation"), result.getString("num_tel"), result.getString("categories"), result.getString("photo"));
            userrecupe = ut.getUsername();
            idrece = ut.getId();
            ne = ut.getNom();
            pne = ut.getPrenom();
            nuse = ut.getUsername();
            me = ut.getMail();
            mpe = ut.getMot_de_passe();
            dne = ut.getDate_naissance();
            loe = ut.getLocalisation();
            nue= ut.getNum_tel();
            cae = ut.getCategories();
            phte = ut.getPhoto();
            

            try {

             /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/ModifProUser.fxml"));
                Parent root = loader.load();
                ModifProUserController rc = loader.getController();
                imageProfEtab.getScene().setRoot(root);   */
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ModifProUser.fxml"))));
               direction=0;

            } catch (IOException ex) {
                System.out.println(ex.getMessage());

            }
        }
    }
    
    private void setNode(Node node) {
        profiletabnode.getChildren().clear();
        profiletabnode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
    
    
}