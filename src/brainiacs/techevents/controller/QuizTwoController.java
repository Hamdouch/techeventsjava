/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.QuizListController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class QuizTwoController implements Initializable {
    @FXML
    private AnchorPane panefornode;

    @FXML
    private JFXCheckBox check1;
    @FXML
    private JFXCheckBox check12;
    @FXML
    private JFXCheckBox check13;
    @FXML
    private JFXCheckBox check23;
    @FXML
    private JFXCheckBox check22;
    @FXML
    private JFXCheckBox check21;
    @FXML
    private JFXCheckBox check41;
    @FXML
    private JFXCheckBox check42;
    @FXML
    private JFXCheckBox check43;
    @FXML
    private JFXCheckBox check33;
    @FXML
    private JFXCheckBox check32;
    @FXML
    private JFXCheckBox check31;
    @FXML
    private JFXButton validerQuiz;
    @FXML
    private JFXButton retourQuiz;
    @FXML
    private Label lab40;
    @FXML
    private Label lab30;
    @FXML
    private Label lab20;
    @FXML
    private Label lab10;
    @FXML
    private Label lab42;
    @FXML
    private Label lab41;
    @FXML
    private Label lab32;
    @FXML
    private Label lab31;
    @FXML
    private Label lab22;
    @FXML
    private Label lab21;
    @FXML
    private Label lab12;
    @FXML
    private Label lab11;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        check1.getText();
        check12.getText();
        check13.getText();
        check21.getText();
        check22.getText();
        check23.getText();
        check31.getText();
        check32.getText();
        check33.getText();
        check41.getText();
        check42.getText();
        check43.getText();
    }    

    @FXML
    private void testQuiz(ActionEvent event) {
        if (check12.isSelected())
        {
            lab11.setText("Vrai");
        }
        else{
           lab12.setText("Faux");
           lab10.setText("La bonne réponse : Oh le bel économiseur d'écran !");

        }
        if (check22.isSelected())
        {
            lab21.setText("Vrai");
        }
        else{
           lab22.setText("Faux");
           lab20.setText("La bonne réponse : IRC");

           

        }
        if (check31.isSelected())
        {
            lab31.setText("Vrai");
        }
        else{
           lab32.setText("Faux");
           lab30.setText("La bonne réponse : AFK Bio\n" +
"AFK : Away From Keyboard Bio : pour raison biologiques");
       

        }
        if (check42.isSelected())
        {
            lab41.setText("Vrai");
        }
        else{
           lab42.setText("Faux");
           lab40.setText("La bonne réponse : Troll");


        }
    }

    @FXML
    private void backHomePage(ActionEvent event) {
          try {
       /*     FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"));

            Parent root= loader.load();
            QuizListController rc= loader.getController();
            
            retourQuiz.getScene().setRoot(root);   */
              setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    
     private void setNode(Node node) {
        
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    }
    

