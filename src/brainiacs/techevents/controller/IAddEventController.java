/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.EventCrud;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.entities.Type;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Duration;
/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IAddEventController implements Initializable {
    
    @FXML
    private VBox panefornode;
    @FXML
    private JFXTextField inesetab;
    @FXML
    private JFXTextField inestitre;
    @FXML
    private JFXTextField inesprix;
    @FXML
    private DatePicker inesdate;
    @FXML
    private JFXTextArea inesdescription;
    @FXML
    private JFXButton inesbtnvalider;
    @FXML
    private Label lbaj;
    @FXML
    private ChoiceBox<String> inescat;
    ObservableList<String> typecatines =FXCollections.observableArrayList("Evénement","Conférence","Compétition");
    private TextField inesloc;
    @FXML
    private JFXTextField pathPicture;
    @FXML
    private JFXButton btnretour;
        public static String imgPathRec="";
   public static Image image ;
    @FXML
    private ChoiceBox<String> ineslocalisation;
    ObservableList<String> typelocines= FXCollections.observableArrayList("Ariana","Bizerte","Beja","Ben Arous",
            "Gafsa","Gabes","Gbeli","Jendouba","Kairouan","Kef","Kassrine",
            "Mahdia","Manouba","Mounastir","Medenin","Nabeul",
            "Sousse","Sidi Bouzid","Sfax","Siliana","Tunis","Tatouin","Touzeur","Zaghouane");
    @FXML
    private Label etabvide;
    @FXML
    private Label titrevide;
    @FXML
    private Label descvide;
    @FXML
    private Label picvide;
    @FXML
    private Label datevide;
    @FXML
    private Label prixvide;
    @FXML
    private Label locvide;
    String etablissementvide="";
    String titlevide="";
    String descriptionvide="";
    String photovide="";
    String dateestvide="";
    String pricevide="";
    String localisationvide="";
    boolean test=false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inescat.setValue("Evénement");
        inescat.setItems(typecatines);
        ineslocalisation.setValue("Tunis");
        ineslocalisation.setItems(typelocines);
        
        
    }    
  //  private Desktop desktop = Desktop.getDesktop();
    @FXML
    private void insertEvent(ActionEvent event) {
        Evenement e = new Evenement();
     
        e.setTitle(inestitre.getText());
        e.setDescription(inesdescription.getText());
        e.setEtablissement(inesetab.getText());
        e.setPrix(parseFloat(inesprix.getText().toString()));
        java.sql.Date daterecup=java.sql.Date.valueOf(inesdate.getValue());
        e.setDate(daterecup);
        e.setLocalisation(ineslocalisation.getValue());
        e.setCategory(Type.valueOf(inescat.getValue()));
        e.setImagepath(pathPicture.getText());
        imgPathRec=pathPicture.getText();

        
        EventCrud ec = new EventCrud();
        
        if(inesetab.getText().equals(""))
        {
            test=true;
            etablissementvide=" Ce champs est vide !! "; 
        }
         if(inestitre.getText().equals(""))
        {test=true;
            titlevide=" Ce champs est vide !! ";
        }
         if(inesdescription.getText().equals(""))
        {test=true;
        descriptionvide=" Ce champs est vide !! ";}
         /*if(inesdate.getValue().toString().equals(""))
        {test=true;
            dateestvide=" Ce champs est vide !! ";
        }*/
          if((String.valueOf(inesprix.getText())).equals(""))
        {test=true;
            pricevide=" Ce champs est vide !! ";
        }
           if(pathPicture.getText().equals(""))
        {test=true;
            photovide=" Ce champs est vide !! ";
        }
          
        if(test)
        {  
            etabvide.setText(etablissementvide);
            titrevide.setText(titlevide);
            descvide.setText(descriptionvide);
            prixvide.setText(pricevide);
            picvide.setText(photovide);
          //  datevide.setText(dateestvide);
        
        
        }
        else
        {
        
        ec.ajouterEvent(e); // insertion dans la base de données
         lbaj.setText("Evénement ajouté avec succès!");
      
        
        try {
          /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAddEvent.fxml")); //chargement de la scene dans le show root
            Parent root = loader.load(); //chargement du show root
            IAddEventController rc= loader.getController(); 
           
            lbaj.getScene().setRoot(root);   */
            
          
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAddEvent.fxml"))));
            
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }}
        
    }
     private static void configureFileChooser(final FileChooser fileChooser){                           
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        ); 
    }
       public void handle(final ActionEvent e) {
                    final FileChooser fileChooser = new FileChooser();
                    configureFileChooser(fileChooser);
                    File file = fileChooser.showOpenDialog(lbaj.getScene().getWindow());
                    if (file != null) {
                        /*openFile(file);*/
                       pathPicture.setText(file.getAbsolutePath());                       
                    }
                }

    @FXML
    private void insertPicture(ActionEvent event) {
                handle(event);

    }


    @FXML
    private void backShow(ActionEvent event) {
        try {
         /*   FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));

            Parent root= loader.load();
            IShowEventController rc= loader.getController();
            
           btnretour.getScene().setRoot(root);   */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    
     private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

} 
        
       
            
            
    
    

