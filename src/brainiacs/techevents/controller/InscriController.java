/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.MyServices.Services;
import static brainiacs.techevents.MyServices.Services.chercherUtilisateurByMail;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.util.StringConverter;

/**
 *
 * @author User
 */
public class InscriController implements Initializable {

    @FXML
    private JFXButton btnins;

    @FXML
    private JFXTextField ftAjNom;
    @FXML
    private JFXTextField ftAjPrenom;
    @FXML
    private JFXTextField ftAjUsername;
    @FXML
    private JFXTextField ftAjMail;
    @FXML
    private JFXTextField ftAjPwd;
    @FXML
    private JFXTextField ftAjLocalisation;
    @FXML
    private JFXTextField ftAjNum;
    @FXML
    private JFXComboBox<String> ftAjCat;
    @FXML
    private DatePicker ftAjdateins;
    public static String maila = "";
    public static String Us = "";
    public static String Mp = "";
    //ObservableList<String> categorielist = FXCollections.observableArrayList("Simple utilisateur","Etablissement");
    @FXML
    private Label emailerreur;
    @FXML
    private Label numtelereur;
    @FXML
    private Label nomerreur;
    @FXML
    private Label prenomerreur;
    @FXML
    private Label usrerreur;
    @FXML
    private Label pwderreur;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ObservableList<String> categorielist = FXCollections.observableArrayList("Simple utilisateur", "Etablissement");
        ftAjCat.setItems(categorielist);
    }

    @FXML
    private void inscrire(ActionEvent event) {
        Utilisateur u = new Utilisateur();

        if (!(ftAjNom.getText().isEmpty())) {
            String masque = "^[a-zA-Z]+$";
            Pattern pattern = Pattern.compile(masque);
            Matcher controler = pattern.matcher(ftAjNom.getText());
            if (!(controler.matches())) {
                nomerreur.setText("Nom invalide");
                nomerreur.setVisible(true);
                //  warning.setVisible(true);
                return;
            }
        } else {
            nomerreur.setVisible(true);
            nomerreur.setText("Champs Nom est vide !");
            nomerreur.setVisible(true);
            //  warning.setVisible(true);
            return;
        }

        if (!(ftAjPrenom.getText().isEmpty())) {
            String masque = "^[a-zA-Z]+$";
            Pattern pattern = Pattern.compile(masque);
            Matcher controler = pattern.matcher(ftAjPrenom.getText());
            if (!(controler.matches())) {
                prenomerreur.setText("Nom invalide");
                prenomerreur.setVisible(true);
                //  warning.setVisible(true);
                return;
            }
        } else {
            prenomerreur.setVisible(true);
            prenomerreur.setText("Champs Nom est vide !");
            prenomerreur.setVisible(true);
            //  warning.setVisible(true);
            return;
        }

        u.setNom(ftAjNom.getText());
        u.setPrenom(ftAjPrenom.getText());
        u.setUsername(ftAjUsername.getText());
        u.setMail(ftAjMail.getText());

        //******************************
        if (!(ftAjMail.getText().isEmpty())) {
            if (!Services.chercherUtilisateurByMail(ftAjMail.getText())) {
                String email_pattern = "^[a-zA-Z0-9_+&*-]+(?:\\."
                        + "[a-zA-Z0-9_+&*-]+)*@"
                        + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                        + "A-Z]{2,7}$";
                Pattern pat = Pattern.compile(email_pattern);
                Matcher matcher = pat.matcher(ftAjMail.getText());

                if (matcher.matches()) {       //if   matcher ne contient pas la format   
                    emailerreur.setVisible(false);
                    emailerreur.setText("Email valide !");

                } else {
                    emailerreur.setVisible(true);
                    emailerreur.setText("Email Format invalide !");

                    return;
                }

            }else{
                emailerreur.setVisible(true);
                    emailerreur.setText("Email existe deja !");
                    return ;
            }
        }
        //*********************************************

        if (ftAjPwd.getText().trim().length() < 8) {

            pwderreur.setText(" Mot de passe tres court !");
            pwderreur.setVisible(true);
            return;
        }

        //*************************************************
        u.setMot_de_passe(ftAjPwd.getText());
        u.setLocalisation(ftAjLocalisation.getText());
        u.setNum_tel(ftAjNum.getText());

        //*******************************************
        if (ftAjNum.getText().trim().length() == 8) {
            int nbChar = 0;
            for (int i = 1; i < ftAjNum.getText().trim().length(); i++) {
                char ch = ftAjNum.getText().charAt(i);

                if (Character.isLetter(ch)) {

                    nbChar++;

                }
                System.out.println(nbChar);
            }

            if (nbChar == 0) {
                numtelereur.setText("Numéro valide");
                numtelereur.setVisible(false);

            } else {
                numtelereur.setText("Numéro de telephone invalide \n"
                        + " Il exist des char");
                numtelereur.setVisible(true);

                return;

            }

        } else {
            numtelereur.setText("invalide number \n"
                    + " Ilfaut 8 chiffres");
            return;
        }

        //************************************************
        u.setCategories(ftAjCat.getValue());
        String path = "/brainiacs/techevents/images/Membre.png";
        u.setPhoto(path);

        //recuperation du date
        String pattern = "dd/MM/yyyy";

        ftAjdateins.setPromptText(u.getDate_naissance());

        ftAjdateins.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        u.setDate_naissance(ftAjdateins.getValue().format(DateTimeFormatter.ISO_DATE)); //valeur du date string

        UserCrud us = new UserCrud();

        us.ajouterUtilisateur(u); // insertion dans la base de données

         //////////chargement de la scene dans le show root
        Mail ct = new Mail();
        maila = u.getMail();
        Us = u.getUsername();
         Mp = u.getMot_de_passe();
        ct.envoyerMessage(maila, Us, Mp);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/login.fxml"));
            Parent root1 = loader.load(); //chargement du show root

            //  LoginController rc = loader.getController();
            ftAjNom.getScene().setRoot(root1);

            // rc.lbNotifAjout.setText("Article ajouté avec succès!");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
