/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ActualiteConsulterController implements Initializable {
    @FXML
    private ImageView imageviewwact;
    @FXML
    private  JFXTextArea desc;
    @FXML
    private AnchorPane panelfornode;
    @FXML
    private ImageView back;
    private String path;
    @FXML
    private Label Categorriefx;
    @FXML
    private JFXTextField datafx;
    @FXML
    private JFXTextField titrefx;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
   
  path = ActualiteForumController.selectionnedAct.getPictures();
  desc.setText(ActualiteForumController.selectionnedAct.getDescription());
  desc.setEditable(false);
  Categorriefx.setText(ActualiteForumController.selectionnedAct.getCategories()); 
 
   datafx.setText(ActualiteForumController.selectionnedAct.getDate().toString());
   titrefx.setText(ActualiteForumController.selectionnedAct.getTitre());
          try {
        
             imageviewwact.setImage(new Image(new FileInputStream(path)));
             
         
         } catch (Exception ex) {
             System.out.println(ex.getMessage());
         }   
        
        
        
        
        
        
    }
    
      private void setNode(Node node) {
    panelfornode.getChildren().clear();
      panelfornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

    @FXML
    private void backToActualite(MouseEvent event) {
        
         try {
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ActualiteForum.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } 
        
    }
    
    
}
