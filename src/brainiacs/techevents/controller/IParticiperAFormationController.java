/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.ParticipantFormationCRUD;
import brainiacs.techevents.entities.ParticipantFormation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author linda
 */
public class IParticiperAFormationController implements Initializable {
      @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXTextField ftCin;
    @FXML
    private JFXTextField ftNom;
    @FXML
    private JFXTextField ftPrenom;
    @FXML
    private JFXTextField ftMail;
    @FXML
    private JFXTextField ftAdresse;
    @FXML
    private JFXTextField ftNum;
    @FXML
    private JFXButton btnValider;
    @FXML
    private Label done;
    @FXML
    private JFXButton btnRetour;
    static ParticipantFormation participantRecup;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }
    
    @FXML
    private void insertParticipant(ActionEvent event) {
      
            ParticipantFormation p = new ParticipantFormation();
            
            p.setCIN(Integer.parseInt(ftCin.getText().toString()));
            p.setNom(ftNom.getText());
            p.setPrenom(ftPrenom.getText());
            p.setMail(ftMail.getText());
            p.setAdresse(ftAdresse.getText());
            p.setNumTelephone(Integer.parseInt(ftNum.getText().toString()));
            ParticipantFormationCRUD pfc = new ParticipantFormationCRUD();
             
        //String videCIN="";
        String videNOM="";
        String videPrenom="";
        String videMail="";
        String videAdresse="";
       // String videNum="";
        boolean test=false;
        
         /* if(ftCin.getText().toString().equals(""))
        {
            test=true;
            videCIN=" Ce champs est vide !! "; 
        }*/
        if (ftCin.getText().length() < 8) {
             Alert alert = new Alert(Alert.AlertType.ERROR);
          alert.setContentText("Veuillez verifier votre CIN  ");
            alert.showAndWait();     
        }
         if(ftNom.getText().equals(""))
        {test=true;
            videNOM=" Ce champs est vide !! ";
        }
         if(ftPrenom.getText().equals(""))
        {test=true;
        videPrenom=" Ce champs est vide !! ";}
         if(ftMail.getText().equals(""))
        {test=true;
            videMail=" Ce champs est vide !! ";
        }
          if(ftAdresse.getText().equals(""))
        {test=true;
            videAdresse=" Ce champs est vide !! ";
        }
           if (ftNum.getText().length() < 8) {
             Alert alert = new Alert(Alert.AlertType.ERROR);
          alert.setContentText("Veuillez verifier votre numéro de telephone ");
            alert.showAndWait();     
        }
       
        if(test)
        {  
            //ftCin.setText(videCIN);
            ftNom.setText(videNOM);
            ftPrenom.setText(videPrenom);
            ftMail.setText(videMail);
            ftAdresse.setText(videAdresse);
        }
        else
        {
            
            pfc.ajouterParticipation(p);
            done.setText("Votre participation est validée");
             try {
        /*    FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IFormation.fxml"));
           
            Parent root = loader.load();
            
                IFormationController rc = loader.getController(); 
            
            
            done.getScene().setRoot(root);  */
            
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IFormation.fxml"))));
            
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
     
    }           

    @FXML
    private void retourParAff(ActionEvent event) {
         try {
        /*    FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IFormation.fxml"));
            Parent root= loader.load();
            IFormationController rc= loader.getController();
            
            btnRetour.getScene().setRoot(root);  */
              setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IFormation.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

   private void setNode(Node node) {
          System.out.println("wslllllllll");
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
}

    

