/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.QuizFourController;
import brainiacs.techevents.controller.IShowEventController;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class QuizListController implements Initializable {
     @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXButton quiz1;
    @FXML
    private JFXButton retourlistquiz;
    @FXML
    private JFXButton quiz2;
    @FXML
    private JFXButton quiz3;
    @FXML
    private JFXButton quiz4;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void redirecthomeshow(ActionEvent event) {
        try {
          /*  FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));

            Parent root= loader.load();
            IShowEventController rc= loader.getController();
            
            retourlistquiz.getScene().setRoot(root);    */
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void passquiztwo(ActionEvent event) {
        try {
       /*     FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizTwo.fxml"));

            Parent root= loader.load();
            QuizTwoController rc= loader.getController();
              quiz2.getScene().setRoot(root); */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizTwo.fxml"))));
            
          
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void passquizthree(ActionEvent event) {
        try {
       /*     FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizThree.fxml"));

            Parent root= loader.load();
            QuizThreeController rc= loader.getController();
                 quiz3.getScene().setRoot(root); */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizThree.fxml"))));
            
       
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void passquizfour(ActionEvent event) {
        try {
        /*    FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizFour.fxml"));

            Parent root= loader.load();
            QuizFourController rc= loader.getController();
            quiz4.getScene().setRoot(root);*/
              setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizFour.fxml"))));
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void passquiztech(ActionEvent event) {
        try {
         /*   FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/QuizTech.fxml"));

            Parent root= loader.load();
            QuizTechController rc= loader.getController();
              quiz1.getScene().setRoot(root); */
            
            
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizTech.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    private void setNode(Node node) {
          System.out.println("wslllllllll");
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
}
