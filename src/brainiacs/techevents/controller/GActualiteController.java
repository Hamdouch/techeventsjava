/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.controller.AdActualiteController;
import static brainiacs.techevents.controller.GActualiteController.stageModifier;
import brainiacs.techevents.crud.GestionActualite;
import brainiacs.techevents.entities.Actualite;
import com.jfoenix.controls.JFXButton;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.ResizeFeatures;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class GActualiteController implements Initializable {
  @FXML
  private ImageView gorss;

    @FXML
    private JFXButton ajoutbtn;

    @FXML
    private ComboBox<String> typeRecherche;
    ObservableList<String> listeTypeRecherche = FXCollections.observableArrayList("Categories", "Titre", "Description", "User", "Tout");
    @FXML
    private TextField RechercheTextField;
    @FXML
    private TableView<Actualite> TableViewActualite;

    @FXML
    private AnchorPane panegestactualite;
    static AnchorPane d;

    //recuperation des donnes de l'act selectionne
    static Actualite selectionnedAct;
    //stage a changé
    static Stage stageModifier;
   

    //creation observablelist pour prendre les DATA importer de la BD:
    ObservableList<Actualite> dataActualitelist;
    ObservableList<Actualite> dataActualitelistrefresh;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stageModifier = new Stage();
        //combobox parameters
        typeRecherche.setValue("Tout");
        typeRecherche.setItems(listeTypeRecherche);

 
        ///
  //      TableColumn<Actualite, Integer> idColumn = new TableColumn<>("ID");
  //     idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
  //      idColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> titreColumn = new TableColumn<>("Titre");
        titreColumn.setCellValueFactory(new PropertyValueFactory<>("titre"));
        titreColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> descColumn = new TableColumn<>("Description");
        descColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        descColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> categColumn = new TableColumn<>("Categories");
        categColumn.setCellValueFactory(new PropertyValueFactory<>("categories"));
        categColumn.setStyle("-fx-alignment: CENTER;");

        ///
        TableColumn<Actualite, String> userColumn = new TableColumn<>("User");
        userColumn.setCellValueFactory(new PropertyValueFactory<>("user"));
        userColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, Date> dateColumn = new TableColumn<>("date");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateColumn.setStyle("-fx-alignment: CENTER;");
        ///
        TableColumn<Actualite, String> screenshotColumn = new TableColumn<>("PHOTO");
        screenshotColumn.setCellValueFactory(new PropertyValueFactory<>("pictures"));
        screenshotColumn.setStyle("-fx-alignment: CENTER;");

        //creation de collumn image : 
        System.out.println("entree");

        Actualite a = new Actualite();
        GestionActualite GA = new GestionActualite();

        dataActualitelist = FXCollections.observableArrayList(GA.afficherToutesLesActualites());
     //   dataActualitelistrefresh = FXCollections.observableArrayList(GA.afficherToutesLesActualites());
        TableViewActualite.setItems(dataActualitelist);
        TableViewActualite.getColumns().addAll(dateColumn,screenshotColumn,titreColumn,userColumn,categColumn, descColumn);
        addButtonDeleteToTable();
        addButtonModifToTable();

      Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>> cellFactoryImage
                = //
                new Callback<TableColumn<Actualite, String>, TableCell<Actualite, String>>() {
            @Override
            public TableCell call(final TableColumn<Actualite, String> param) {
                final TableCell<Actualite, String> cell = new TableCell<Actualite, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            try {
                                ImageView imagev = new ImageView(new Image(new FileInputStream(item)));
                                imagev.setFitHeight(120);
                                imagev.setFitWidth(200);
                                setGraphic(imagev);
                                setText(null);
                                //System.out.println(item);
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                };
                
                cell.setOnMouseClicked((MouseEvent event2)
                        -> {
                    if (event2.getClickCount() == 1) {
                        if (TableViewActualite.getSelectionModel().getSelectedItem() != null && !TableViewActualite.getSelectionModel().getSelectedItem().getPictures().contains("null")) {
                            Stage window = new Stage();
//
                            window.setMinWidth(250);
                            ImageView imagevPOPUP;
                            try {
                                imagevPOPUP = new ImageView(new Image(new FileInputStream( TableViewActualite.getSelectionModel().getSelectedItem().getPictures())));
                                imagevPOPUP.setFitHeight(576);
                            imagevPOPUP.setFitWidth(1024);
                            
                            VBox layout = new VBox(10);
                            layout.getChildren().addAll(imagevPOPUP);
                            layout.setAlignment(Pos.CENTER);

                            //Display window and wait for it to be closed before returning
                            Scene scene = new Scene(layout);
                            window.setScene(scene);
                            window.show();
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.getMessage());
                            }
                           

                        }
                    }
                });
                
                
                return cell;
            }
        };

       screenshotColumn.setCellFactory(cellFactoryImage);
        

    

    }

    @FXML
    private void adddirectbtn(ActionEvent event) {
        try {

            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/adActualite.fxml"))));

        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        }

    }

    private void addButtonDeleteToTable() {
        TableColumn<Actualite, Void> colBtn = new TableColumn("Supprimer Actualité");

        Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>> cellFactory = new Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>>() {
            @Override
            public TableCell<Actualite, Void> call(final TableColumn<Actualite, Void> param) {
                final TableCell<Actualite, Void> cell = new TableCell<Actualite, Void>() {

                    private final JFXButton btn = new JFXButton("Supprimer");

                    {

                        btn.setOnAction((ActionEvent event) -> {
                            Actualite act = getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Suppression");
                            alert.setHeaderText(null);
                            alert.setContentText("Vous voulez vraiment supprimer l'actualite de l'ID " + act.getId() + " et de Titre <" + act.getTitre() + "> ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                Notifications n = Notifications.create()
                                        .title("Succès")
                                        .text("Suppression d'actualite de titre " + act.getTitre() + " avec succès")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(3));
                                n.showInformation();
                                GestionActualite GA = new GestionActualite();
                                GA.supprimerActualite(act); //supprimer T3amlet

                            } else {
                                Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("Suppression d'actualite annuller")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(3));
                                n.showInformation();
                            }

                            try {

                                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        TableViewActualite.getColumns().add(colBtn);

    }

    private void addButtonModifToTable() {
        TableColumn<Actualite, Void> colBtn = new TableColumn("Modifier Actualite");

        Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>> cellFactory = new Callback<TableColumn<Actualite, Void>, TableCell<Actualite, Void>>() {
            @Override
            public TableCell<Actualite, Void> call(final TableColumn<Actualite, Void> param) {
                final TableCell<Actualite, Void> cell = new TableCell<Actualite, Void>() {

                    private final JFXButton btn = new JFXButton("Modifier");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            selectionnedAct = getTableView().getItems().get(getIndex());

                            try {

                                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/MDActualite.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        TableViewActualite.getColumns().add(colBtn);

    }

    @FXML
    private void listRecherche() {
        ArrayList arrayList = null;
        GestionActualite GA = new GestionActualite();

        if (typeRecherche.getValue().equals("Categories")) {
            arrayList = (ArrayList) GA.rechercheActualite(typeRecherche.getValue(), RechercheTextField.getText());
        }
        if (typeRecherche.getValue().equals("Titre")) {
            arrayList = (ArrayList) GA.rechercheActualite(typeRecherche.getValue(), RechercheTextField.getText());
        }
        if (typeRecherche.getValue().equals("Description")) {
            arrayList = (ArrayList) GA.rechercheActualite(typeRecherche.getValue(), RechercheTextField.getText());
        }
        if (typeRecherche.getValue().equals("User")) {
            arrayList = (ArrayList) GA.rechercheActualite(typeRecherche.getValue(), RechercheTextField.getText());
        }
        if (typeRecherche.getValue().equals("Tout")) {
            arrayList = (ArrayList) GA.rechercheActualite(typeRecherche.getValue(), RechercheTextField.getText());
        }

        //("Categories", "Titre" ,"Description","User","Tout");
        ObservableList observableList = FXCollections.observableArrayList(arrayList);
        TableViewActualite.setItems(observableList);

    }
      @FXML
    void gopagerss(MouseEvent event) {
      try {
          setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/RssFeedmenu.fxml"))));
      } catch (IOException ex) {
          System.out.println(ex.getMessage());
      }
    }


    private void setNode(Node node) {
        panegestactualite.getChildren().clear();
        panegestactualite.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

}
