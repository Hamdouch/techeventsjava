/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.orsonpdf.PDFDocument;
import brainiacs.techevents.crud.ArticleCrud;
import brainiacs.techevents.entities.Article;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import javax.swing.JFileChooser;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AjoutArticleController implements Initializable {
    @FXML
    private AnchorPane panelfornodee;
    @FXML
    private JFXTextField ftAjAuteur;
    @FXML
    private JFXTextField ftAjJournal;
    @FXML
    private JFXTextField ftAjTitre;
    @FXML
    private JFXTextField ftAjPath;
    @FXML
    private JFXButton btValAjArticle;
    @FXML
    private Label lbNotifAjout;
    @FXML
    private JFXButton btAjRetour;
    @FXML
    private Label lbvide;
    @FXML
    private Label lbtitreVide;
    @FXML
    private Label lbjournalvide;
    @FXML
    private Label lbauteurVide;

    public JFXTextField getFtAjAuteur() {
        return ftAjAuteur;
    }

    public JFXTextField getFtAjJournal() {
        return ftAjJournal;
    }

    public JFXTextField getFtAjTitre() {
        return ftAjTitre;
    }

    public JFXTextField getFtAjPath() {
        return ftAjPath;
    }

    public JFXButton getBtValAjArticle() {
        return btValAjArticle;
    }

    public void setFtAjAuteur(String ftAjAuteur) {
        this.ftAjAuteur.setText(ftAjAuteur);
    }

    public void setFtAjJournal(String ftAjJournal) {
        this.ftAjJournal.setText(ftAjJournal);
    }

    public void setFtAjTitre(String ftAjTitre) {
        this.ftAjTitre.setText(ftAjTitre);
    }

    public void setFtAjPath(String ftAjPath) {
        this.ftAjPath.setText(ftAjPath);
    }

    public void setBtValAjArticle(JFXButton btValAjArticle) {
        this.btValAjArticle = btValAjArticle;
    }

    /**
     * Initializes the controller class.
     */
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void validerAjoutArticle(ActionEvent event) {
        Article a = new Article();
        a.setAuteur(ftAjAuteur.getText());
        a.setJournal(ftAjJournal.getText());
        a.setTitre(ftAjTitre.getText());
        a.setEmplacement(ftAjPath.getText());
        
        String videaut="";
        String videtitre="";
        String videjournal="";
        String videpath="";
        boolean test=false;
        
        ArticleCrud ac = new ArticleCrud();
        if(ftAjAuteur.getText().equals(""))
        {
            test=true;
            videaut=" Ce champs est vide !! "; 
        }
         if(ftAjJournal.getText().equals(""))
        {test=true;
            videjournal=" Ce champs est vide !! ";
        }
         if(ftAjTitre.getText().equals(""))
        {test=true;
        videtitre=" Ce champs est vide !! ";}
         if(ftAjPath.getText().equals(""))
        {test=true;
            videpath=" Ce champs est vide !! ";
        }
        if(test)
        {  
            lbvide.setText(videpath);
            lbauteurVide.setText(videaut);
            lbjournalvide.setText(videjournal);
            lbtitreVide.setText(videtitre);
        
        
        }
        else
        {
         ac.ajouterArticle(a); // insertion dans la base de données
        
        /*Partie envoi mail*/
   /* try{String host ="smtp.gmail.com" ;
            String user = "mahmoudbeher.chebil@esprit.tn";
            String pass = "chichatoffah";
            String to = "hamdi.messaoudi@esprit.tn";
            String from = "mahmoudbeher.chebil@esprit.tn";
            String subject = "Article reccu";
            File sourceFile = new File("c:/testPDF.pdf");
	    FileInputStream fis = new FileInputStream(sourceFile);
	    BufferedInputStream bis = new BufferedInputStream(fis);
	    long l = sourceFile.length();
 
	    // Préparation du flux de sortie
	    FileOutputStream fos = new FileOutputStream("c:/sortiePDF.pdf");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
 
	    // Copie des octets du flux d'entrée vers le flux de sortie
	    for(long i=0;i<l;i++) {
                bos.write(bis.read());
	    }
 
	    // Fermeture des flux de données
	    bos.flush();
	    bos.close();
	    
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setContent(l, from);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           System.out.println("message send successfully");
        }catch(Exception ex)
        {
            System.out.println(ex);
            System.out.println("no");
        
    }*/
        /*fin envoi */
    try{
        /*
       FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/AjoutArticle.fxml")); //chargement de la scene dans le show root
        
       
            Parent root = loader.load(); //chargement du show root
            
            AjoutArticleController rc= loader.getController(); //le contrôleur de result.fxml (second show)
           
            
            
            lbNotifAjout.getScene().setRoot(root);   */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/AjoutArticle.fxml"))));
            lbNotifAjout.setText("Article ajouté avec succès!");
           
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }}
            
       
    }

    @FXML
    private void retourAjArticleVersGestionArticle(ActionEvent event) {
        try {
           /* FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"));
            Parent root= loader.load();
            GestionDesArticlesController rc= loader.getController();
            btAjRetour.getScene().setRoot(root);  */
            if(InterfaceArticleEtablissementController.direction==1){
                
                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/InterfaceArticleEtablissement.fxml"))));
            }else{
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"))));
            }
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
        
    }

    @FXML
    private void insertFile(ActionEvent event) {
JFileChooser chooser=new JFileChooser();
chooser.showOpenDialog(null);
File f=chooser.getSelectedFile();
String filename=f.getAbsolutePath();
ftAjPath.setText(filename);
    }
      private void setNode(Node node) {
        panelfornodee.getChildren().clear();
        panelfornodee.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
}
