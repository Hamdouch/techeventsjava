/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import brainiacs.techevents.crud.FormationCRUD;
import brainiacs.techevents.entities.Formation;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author linda
 */
public class IAfficherFormationController implements Initializable {
    @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXButton btnconsulterParticipant;

    @FXML
    private TableColumn<Formation, String> CTitre;
    @FXML
    private TableColumn<Formation, String> Cetab;
    @FXML
    private TableColumn<Formation, String> Cdesc;
    @FXML
    private TableColumn<Formation, Date> CdateDebut;
    @FXML
    private TableColumn<Formation, Date> CDateFin;
    @FXML
    private TableView<Formation> tabFormation;
     @FXML
    private TableColumn<Formation, Float> CPrix;
     
    ObservableList<Formation> afficherFormation;
    @FXML
    private JFXButton btAjFor;
    @FXML
    private TextField tfRchTitre;
    static Formation formationRecup;
    @FXML
    private JFXButton btnStat;
    CategoryAxis xAxis;
    NumberAxis yAxis;
    LineChart<String, Number> lineChart;
    XYChart.Series<String, Number> data;
   
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        CTitre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        Cetab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        Cdesc.setCellValueFactory(new PropertyValueFactory<>("description"));
        CdateDebut.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
        CDateFin.setCellValueFactory(new PropertyValueFactory<>("date_fin"));
        CPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        
        addButtonDeleteToTable();
        addButtonUpdateToTable();
        
        
        Formation f = new Formation();
        FormationCRUD fc = new FormationCRUD();
        
        afficherFormation = FXCollections.observableArrayList(fc.afficherFormation());
        tabFormation.setItems(afficherFormation);
        
    }    
    private void addButtonDeleteToTable() {
        TableColumn<Formation, Void> colBtn = new TableColumn("Supprimer Formation");

        Callback<TableColumn<Formation, Void>, TableCell<Formation, Void>> cellFactory = new Callback<TableColumn<Formation, Void>, TableCell<Formation, Void>>() {
            @Override
            public TableCell<Formation, Void> call(final TableColumn<Formation, Void> param) {
                final TableCell<Formation, Void> cell = new TableCell<Formation, Void>() {

                    private final Button btn = new Button("Supprimer");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Formation f = getTableView().getItems().get(getIndex());
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Suppression");
                            alert.setHeaderText(null);
                            alert.setContentText("Voulez vous vraiment supprimer cette formation ?");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK) {
                                FormationCRUD fc = new FormationCRUD();
                                fc.supprimerFormation(f.getId()); 

                            }

                            try {

                         /*       FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
                                Parent root = loader.load();
                                IAfficherFormationController rc = loader.getController();
                               btAjFor.getScene().setRoot(root);     */
                                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabFormation.getColumns().add(colBtn);

    }
    private void addButtonUpdateToTable() {
        TableColumn<Formation, Void> colBtn = new TableColumn("Modifier Formation");

        Callback<TableColumn<Formation, Void>, TableCell<Formation, Void>> cellFactory = new Callback<TableColumn<Formation, Void>, TableCell<Formation, Void>>() {
            @Override
            public TableCell<Formation, Void> call(final TableColumn<Formation, Void> param) {
                final TableCell<Formation, Void> cell = new TableCell<Formation, Void>() {

                    private final Button btn = new Button("Modifier");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            formationRecup=getTableView().getItems().get(getIndex());

                                 try {

                         /*       FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IModifierFormation.fxml"));
                                Parent root = loader.load();
                                IModifierFormationController rc = loader.getController();
                               btn.getScene().setRoot(root);   */
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IModifierFormation.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tabFormation.getColumns().add(colBtn);

    }

    @FXML
    private void insertForm(ActionEvent event) {
        try {
        /*    FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAjoutFormation.fxml"));
            Parent root= loader.load();
            IAjoutFormationController rc= loader.getController();
            btAjFor.getScene().setRoot(root);   */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAjoutFormation.fxml"))));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }

    @FXML
    private void chercher(KeyEvent event) {
        
        CTitre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        Cetab.setCellValueFactory(new PropertyValueFactory<>("etablissement"));
        Cdesc.setCellValueFactory(new PropertyValueFactory<>("description"));
        CdateDebut.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
        CDateFin.setCellValueFactory(new PropertyValueFactory<>("date_fin"));
        CPrix.setCellValueFactory(new PropertyValueFactory<>("prix"));
        
        Formation f=new Formation();
        FormationCRUD fc =new FormationCRUD();
        
        afficherFormation=FXCollections.observableArrayList(fc.chercherTitreFormation(tfRchTitre.getText()));
        tabFormation.setItems(afficherFormation);


    }

    @FXML
    private void insertStat(ActionEvent event) {
             HBox root = new HBox();
        Scene scene = new Scene(root, 500, 450);
        
        xAxis = new CategoryAxis();
        xAxis.setLabel("Nom de l'établissement");
        yAxis = new NumberAxis();
        yAxis.setLabel("Nombre de formation");   
        lineChart = new LineChart<String, Number>(xAxis, yAxis);
        lineChart.setTitle("Statistique des établissement en fonction \n de nombre de formation crée");
        data = new XYChart.Series<>();

        FormationCRUD listPost = new FormationCRUD();
 
        List<Formation> list = listPost.selectAllFormations();
    System.out.println(list);
        for (Formation p : list) {
            data.getData().add(new XYChart.Data<String, Number>(
                    p.getTitre(),
                    
                    listPost.chercherEtabFormation(p.getTitre()).size()

            ));

            lineChart.getData().add(data);
        }

        root.getChildren().add(lineChart);

        Stage primaryStage = new Stage();

        primaryStage.setTitle("Statistique");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
    }
    @FXML
    void goconsulter(ActionEvent event) {
        try {
            setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherParticipant.fxml"))));
        } catch (IOException ex) {
            Logger.getLogger(IAfficherFormationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     private void setNode(Node node) {
         
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
    
    
    
    
    
        
    }
    

