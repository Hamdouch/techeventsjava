/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.connexion.MyConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IPayController implements Initializable {
       @FXML
    private JFXButton btnback;
      @FXML
    private VBox panefornode;

    @FXML
    private JFXTextField pnum;
    @FXML
    private JFXTextField pcode;
    @FXML
    private JFXButton btnpay;
    @FXML
    private Label paysuccess;
    @FXML
    private Label payechec;
    private static Float recsolde;
    String numvide="";
    String codevide="";
    boolean test=false;
    @FXML
    private Label lbnumvide;
    @FXML
    private Label lbcodevide;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void insertCode(ActionEvent event) throws SQLException {
        String requete = "select code,solde from pay where numCarte=?"; //selection du code et solde par rapport au numéro de la carte bancaire
        String requete2 = "update pay set solde=solde-?  where numCarte=?"; //Mise à jour au nouveau solde dans le compte après le payement
        MyConnection mycnx = MyConnection.getMyConnection();
        if (pnum.getText().equals("")) {
            test = true;
            numvide = " Ce champs est vide !! ";
        }
        if (pcode.getText().equals("")) {
            test = true;
            codevide = " Ce champs est vide !! ";
        }
        if(test){
            lbcodevide.setText("Ce champ est vide");
            lbnumvide.setText("Ce champ est vide");
        }else{
        try {

            PreparedStatement pst = mycnx.getCnx().prepareStatement(requete);
            pst.setString(1, pnum.getText());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                if (pcode.getText().equals(rs.getString(1))) { //test si le code saisie est correcte
                    if (IShowEventController.prixrecup <= rs.getFloat(2)) { //test si le solde est supérieur au prix de l'événement que le participant souhaite payer
                        paysuccess.setText("Payement réussi!"); //les deux test sont correctes
                        PreparedStatement pst2 = mycnx.getCnx().prepareStatement(requete2);
                        pst2.setFloat(1, IShowEventController.prixrecup);
                        pst2.setString(2, pnum.getText());
                        pst2.executeUpdate();

                        pst.setString(1, pnum.getText());
                    } else {
                        payechec.setText("Votre solde est insuffisant pour participer");//code correcte mais le solde est insffisant pour faire le payement
                    }
                } else {
                    payechec.setText("Payement échoué : vérifiez vos informations"); //le code est incorrecte
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }}
    
    
    
    @FXML
    void insertback(ActionEvent event) {
           try {
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowUserEvent.fxml"))));
           } catch (IOException ex) {
               Logger.getLogger(IPayController.class.getName()).log(Level.SEVERE, null, ex);
           }

    }
     private void setNode(Node node) {
        panefornode.getChildren().clear();
       panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

}
