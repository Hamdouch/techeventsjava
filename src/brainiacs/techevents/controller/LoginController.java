/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.controller.Mail;
import brainiacs.techevents.crud.UserCrud;
import brainiacs.techevents.entities.Utilisateur;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import brainiacs.techevents.techniques.InputValidation;
import javafx.animation.FadeTransition;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 *
 * @author User
 */
public class LoginController implements Initializable {
    @FXML
    private JFXTextField labelusername;
    @FXML
    private JFXPasswordField labelpassword;
    @FXML
    private JFXButton btnlogin;
    @FXML
    private ImageView imgProgress;
    @FXML
    private Hyperlink forgetpw;
    public static String maila = "";
    public static String Mp = "";
    public static final String ACCOUNT_SID = "AC6c72af9c639d682719d96d3abc3ee138";
    public static final String AUTH_TOKEN = "46843e93e79f7ce2e2917e230192fe00";
    @FXML
    private JFXButton btninscription;
    public static String username ="";
    public static String ph="";
    public static Utilisateur ute ;
    public static int valeurauth;
     @FXML
    private AnchorPane nodelogin;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        imgProgress.setVisible(false);
    }

    public void Al(String txt) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Information Dialog");
        alert.setContentText(txt);
        alert.showAndWait();
        
    }

    @FXML
    private void handleLoginButtonAction(ActionEvent event) throws InterruptedException {
        imgProgress.setVisible(true);
        Utilisateur myServices = new Utilisateur();
        String mdp = labelpassword.getText();
        username = labelusername.getText();

        String errorMessage = "";

        if (username == null || username.length() == 0) {
            errorMessage += "Username invalide \n";
        }

        if (mdp == null || mdp.length() == 0) {
            errorMessage += "Mot de passe invalide \n";
        }

        Connection connection;
        PreparedStatement ps;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/techevents", "root", "");
            //ps = connection.prepareStatement("SELECT `username`, `mot_de_passe`,`categories` FROM `membres` WHERE `username` = ? AND `mot_de_passe` = ?");
            ps = connection.prepareStatement("SELECT * FROM `membres` WHERE `username` = ? AND `mot_de_passe` = ?");
            ps.setString(1, String.valueOf(labelusername.getText()));
            ps.setString(2, String.valueOf(labelpassword.getText()));

            ResultSet result = ps.executeQuery();

            if (result.next()) {
                ute = new Utilisateur(result.getInt("id"), result.getString("nom"), result.getString("prenom"), result.getString("username"), result.getString("mail"), result.getString("mot_de_passe"), result.getString("date_naissance"), result.getString("localisation"), result.getString("num_tel"), result.getString("categories"),result.getString("photo"));
                ph = ute.getPhoto();
                Al("Authentification réussite !");
                if (ute.getCategories().equals("Admin")) {
                    try { //setStage("Administration.fxml"); //ajout
                       /* FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"));
                        Parent root = loader.load();
                        btnlogin.getScene().setRoot(root);  */
                        setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/FXMLDocument.fxml"))));
                        valeurauth=0;
                        
                        
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }else if(ute.getCategories().equals("Simple utilisateur")){
                    try { 
                      /*  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"));
                        Parent root = loader.load();
                        btnlogin.getScene().setRoot(root);   */
                        setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/FXMLDocument.fxml"))));
                         valeurauth=1;
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
                else if (ute.getCategories().equals("Etablissement")){
                     try { 
                      /*  FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/ProfilEtab.fxml"));
                        Parent root = loader.load();
                        btnlogin.getScene().setRoot(root);  */
                         setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/FXMLDocument.fxml"))));
                          valeurauth=2;
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
                    
                }

             else {
               Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error valeur");
                alert.setHeaderText("Invalide input");
                alert.setContentText(errorMessage);
                alert.show();
                
            }
        } catch (SQLException ex) {
            System.out.println("Errer");
            System.out.println(ex);
        }

    }

    @FXML
    private void On_forgetpw(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Mot de passe oublié ?");
        dialog.setHeaderText("Veuillez saisir votre Mail ");
        dialog.setContentText("Saissizer votre Mail:");

// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            System.out.println(result.get());

                Utilisateur U = null;
                UserCrud us = new UserCrud();

                U = us.findbyMail(result.get());
                if (U == null) {
                    System.out.println("uuu");
                }
                if (us.findbyMail(result.get()) == null) {
                    Alert alert = new InputValidation().getAlert("ERREUR", "Attention! Utilisateur n'existe pas!");
                    alert.showAndWait();
                } else {

                    Mail ct = new Mail();
                    maila = result.get();
                    Mp = U.getMot_de_passe();
                    ct.envoyerMessageForgetpass(maila, Mp);
                }
            }
        }
    

    @FXML
    void btninscription(ActionEvent event) throws IOException {
     /*   FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/Inscri.fxml"));
        Parent root = loader.load();
        btnlogin.getScene().setRoot(root);   */
         setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Inscri.fxml"))));
    }
    
    
     private void setNode(Node node) {
       nodelogin.getChildren().clear();
        nodelogin.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    
}
