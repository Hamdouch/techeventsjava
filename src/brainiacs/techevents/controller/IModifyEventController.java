/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.EventCrud;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.entities.Type;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class IModifyEventController implements Initializable {
      @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXButton inesbtnvalider;
    private JFXTextField inesloc;
    @FXML
    private JFXTextField inesprix;
    @FXML
    private JFXTextArea inesdescription;
    @FXML
    private JFXTextField inestitre;
    @FXML
    private JFXTextField inesetab;
    @FXML
    private ChoiceBox<String> inescat;
    @FXML
    private Label lbaj;
    @FXML
    private DatePicker inesdate;
    @FXML
    private JFXButton inesbtnretour;
    ObservableList<String> typecatines =FXCollections.observableArrayList("Evénement","Conférence","Compétition");
    @FXML
    private JFXTextField pathPicture;

    @FXML
    private ChoiceBox<String> ineslocalisation;
    ObservableList<String> typelocines= FXCollections.observableArrayList("Ariana","Bizerte","Beja","Ben Arous",
            "Gafsa","Gabes","Gbeli","Jendouba","Kairouan","Kef","Kassrine",
            "Mahdia","Manouba","Mounastir","Medenin","Nabeul",
            "Sousse","Sidi Bouzid","Sfax","Siliana","Tunis","Tatouin","Touzeur","Zaghouane");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inestitre.setText(IShowEventController.recup.getTitle());
        inesdescription.setText(IShowEventController.recup.getDescription());
        inesetab.setText(IShowEventController.recup.getEtablissement());
        //inesloc.setText(IShowEventController.recup.getLocalisation());
        ineslocalisation.setValue(IShowEventController.recup.getLocalisation());
        inesprix.setText(Float.toString(IShowEventController.recup.getPrix()));
        inesdate.setValue(IShowEventController.recup.getDate().toLocalDate());
        inescat.setValue(IShowEventController.recup.getCategory().toString());
          inescat.setValue("Evénement");
        inescat.setItems(typecatines);
        ineslocalisation.setValue("Tunis");
        ineslocalisation.setItems(typelocines);
        
               
    }

    @FXML
    private void returnShow(ActionEvent event) { 
        try {
          /*  FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));
            Parent root= loader.load();
            IShowEventController rc= loader.getController();
            
            inesbtnretour.getScene().setRoot(root);  */
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    

    @FXML
    private void modifyMyEvent(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Modification");
                            alert.setHeaderText(null);
                            alert.setContentText("voulez vous vraiment modifier cet événement ? ");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK)
                            {
                                EventCrud ec=new EventCrud();
                                ec.modifierEvent(IShowEventController.recup.getId(),inestitre.getText(),
                                        inesdescription.getText(),ineslocalisation.getValue(),inesetab.getText(),
                                        Float.parseFloat(inesprix.getText()),Date.valueOf(inesdate.getValue()),
                                        Type.valueOf(inescat.getValue()),pathPicture.getText());

                                   try {

                            /*    FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));
                                Parent root = loader.load();
                               IShowEventController rc = loader.getController();
                               inestitre.getScene().setRoot(root);  */
                                       setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
         }
    
    }
    private static void configureFileChooser(final FileChooser fileChooser){                           
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        ); 
    }
       public void handle(final ActionEvent e) {
                    final FileChooser fileChooser = new FileChooser();
                    configureFileChooser(fileChooser);
                    File file = fileChooser.showOpenDialog(lbaj.getScene().getWindow());
                    if (file != null) {
                        /*openFile(file);*/
                        pathPicture.setText(file.getAbsolutePath());
                    }
                }

    @FXML
    private void insertPic(ActionEvent event) {
                        handle(event);
    }
    
    
     private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
   
    }
    

