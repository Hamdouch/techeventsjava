/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import brainiacs.techevents.gui.MainClass;
import static brainiacs.techevents.controller.GActualiteController.stageModifier;
import brainiacs.techevents.crud.GestionActualite;
import brainiacs.techevents.entities.Actualite;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import java.io.File;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AdActualiteController implements Initializable {
    

    ObservableList<String> CategoriesStatuslist = FXCollections.observableArrayList("Evenement", "Article", "Formation","TechEvents");

     @FXML
    private AnchorPane paneajoutact;
    @FXML
    private JFXTextField textuser;

    @FXML
    private JFXTextField texttitre;

    @FXML
    private ChoiceBox btncateg;

    
    @FXML
    private Label labelajresultat;
    @FXML
    private JFXButton retourbtn;
     @FXML
    private JFXButton btnadact;

    @FXML
    private JFXTextArea textdesc;
    @FXML
    private ImageView screenshotView;
    @FXML
    private JFXButton image;
    private String path="";
    File selectedFile;
    @FXML
    private Label photo;
    List<String> lstFile;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
 lstFile = new ArrayList<>();
        lstFile.add("*.png");
        lstFile.add("*.jpg");
        btncateg.setValue("Evenement");
        btncateg.setItems(CategoriesStatuslist);
        
        
          screenshotView.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        // Dropping over surface
        screenshotView.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    path = null;
                    for (File file : db.getFiles()) {
                        path = file.getName();
                        selectedFile = new File(file.getAbsolutePath());
                        System.out.println("Drag and drop file done and path=" + file.getAbsolutePath());//file.getAbsolutePath()="C:\Users\X\Desktop\ScreenShot.6.png"
                        screenshotView.setImage(new Image("file:" + file.getAbsolutePath()));
//                        screenshotView.setFitHeight(150);
//                        screenshotView.setFitWidth(250);
                        image.setText(path);
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });    

        screenshotView.setImage(new Image("/brainiacs/techevents/images/drag-drop.gif"));
    
    
        
        
        
    }

    @FXML
    void insertactualite(ActionEvent event) {
        try {
            //set DAte de temps Réel
            Date dateutil = new java.util.Date();
            Date datesql = new java.sql.Date(dateutil.getTime());

            //set les formation d'Actualite
            Actualite act = new Actualite();
            act.setTitre(texttitre.getText());
            act.setUser(textuser.getText());
            act.setDescription(textdesc.getText());
            act.setCategories(btncateg.getValue().toString());
            act.setDate(datesql);
            act.setPictures(path);
        //appel au CRUD pour utiliser l'Ajout
       if(act.getTitre().equals("")){
            Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("veuillez remplir le champ titre")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                n.showInformation();
                               
       }
       else if(act.getDescription().equals("")){
         Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("veuillez remplir le champ  Description ")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                n.showInformation();
       }
       else if(act.getUser().equals("")){
            Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("veuillez remplir le champ  Utilisateur ")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                       n.showInformation();
           }
 
       
         else  if (act.getTitre().equals("")  && act.getDescription().equals("") && act.getUser().equals("")) {
              Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text(" Aucun champ saisie ")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                       n.showInformation();
             

                
            }  else if(!act.getPictures().equals("")){
                GestionActualite GA = new GestionActualite();
                GA.ajouterActualite(act);
                Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text(" Actualite Ajouter ")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                       n.showInformation();

                setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));

            }else {
                Notifications n = Notifications.create()
                                        .title("TechEvents")
                                        .text("aucune image saisie ")
                                        .graphic(null)
                                        .position(Pos.TOP_CENTER)
                                        .hideAfter(Duration.seconds(2));
                                n.showInformation();
            }   

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
      

    }

    @FXML
    private void retourgactualite(ActionEvent event) {
       try {
         
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"))));
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        } 
        
     }
     private void setNode(Node node) {
        paneajoutact.getChildren().clear();
        paneajoutact.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

    @FXML
     private void image(ActionEvent event) throws MalformedURLException {
         FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home") + "\\Desktop"));
        fc.setTitle("Veuillez choisir l'image");
        fc.getExtensionFilters().addAll(
                //new FileChooser.ExtensionFilter("Image", ".jpg", ".png"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg")
        );
        selectedFile = fc.showOpenDialog(null);

        if (selectedFile != null) {

            path = selectedFile.getAbsolutePath();
            
            
//    
            image.setText(selectedFile.getName());
            Image imagea = new Image(selectedFile.toURI().toString());
           screenshotView.setImage(imagea) ;

        }
    }
    

    @FXML
    private void keyphoto(InputMethodEvent event) {
    }

  









}


