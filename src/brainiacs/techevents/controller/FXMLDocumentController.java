/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import static brainiacs.techevents.controller.GActualiteController.stageModifier;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class FXMLDocumentController implements Initializable {
      @FXML
    private AnchorPane bigpaneldeco;
    @FXML
    private JFXButton btnHome;
    @FXML
    private JFXButton btnProfile;
    @FXML
    private AnchorPane holderPane;

    @FXML
    private JFXButton btnActualite;
    @FXML
    private JFXButton btnEvnt;
    @FXML
    private JFXButton btnForm;
    @FXML
    private JFXButton btnArticle;
    @FXML
    private JFXButton btndeconnect;
    //les valeurs static pour l'apppel des pages 
    public static AnchorPane actualiteAdmin,actualiteforum,article,
            profilGSADMIN,profiletab,profiluser,formationadmin,formationuser,formationetab,
            articleuser,articleetab,hometeam,Quizpage,Chatpage,Evenadminetab,Evenuser;

 private static AnchorPane oldone;
    @FXML
    private JFXButton btnquiz;
    @FXML
    private JFXButton btnchat;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       try {
            // TODO
           actualiteAdmin = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GActualite.fxml"));
           actualiteforum =FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ActualiteForum.fxml"));
           
           
           article = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"));
           articleuser = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/InterfaceArticleUser.fxml"));
           articleetab = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/InterfaceArticleEtablissement.fxml"));
           
           
           
           profilGSADMIN = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/Administration.fxml"));
           profiluser = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilUser.fxml"));
           profiletab = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/ProfilEtab.fxml"));
           
           
           
           formationadmin = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAdminFormation.fxml"));
           formationuser =  FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IFormation.fxml"));
          formationetab =  FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
          
          hometeam =FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/team.fxml"));
          
          Quizpage=  FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/QuizList.fxml"));
          
          Chatpage = FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/chat.fxml"));
          
          
          Evenadminetab=FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowEvent.fxml"));
          Evenuser=FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IShowUserEvent.fxml")); 
       
       
        } catch (IOException ex) {
           ex.getMessage();
        }
    }    

      private void setNode(Node node) {
          System.out.println("wslllllllll");
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
      private void setNodeprincipale(Node node) {
          System.out.println("wslllllllll");
         bigpaneldeco.getChildren().clear();
        bigpaneldeco.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    


    @FXML
    private void switchProfile(ActionEvent event) {
        if(LoginController.valeurauth==0){
            setNode(profilGSADMIN); 
        }else if(LoginController.valeurauth==1){
            setNode(profiluser);
        }else {
            setNode(profiletab);
        }
       
    }



    @FXML
    public void switchActualite(ActionEvent event) {
if (LoginController.valeurauth==0){
        setNode(actualiteAdmin);
}else {
             setNode(actualiteforum);
      }
  
    }
    @FXML
    private void switchHome(ActionEvent event) {
        setNode(hometeam);
    }

    @FXML
    private void switchEvenement(ActionEvent event) {
        if (LoginController.valeurauth==1){
            setNode(Evenuser);
            
        }else {
             setNode(Evenadminetab);
        }
        
    }

    @FXML
    private void switchFormation(ActionEvent event) {
        if(LoginController.valeurauth==0){
             setNode(formationadmin); 
        }else if(LoginController.valeurauth==1){
            setNode(formationuser);
     }else {
             setNode(formationetab);
        }
       
        
        
    }

    @FXML
    private void switchArticle(ActionEvent event) {
        if(LoginController.valeurauth==0){
            setNode(article); 
        }else if(LoginController.valeurauth==1){
            setNode(articleuser);
        }else {
            setNode(articleetab);
        }
       
        
       
    }

    @FXML
    private void switchdeconnect(ActionEvent event) {
      Stage stage = (Stage) btndeconnect.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void switchQuiz(ActionEvent event) {
        setNode(Quizpage);
    }

    @FXML
    private void switchChat(ActionEvent event) {
        setNode(Chatpage);
    }
    
}
