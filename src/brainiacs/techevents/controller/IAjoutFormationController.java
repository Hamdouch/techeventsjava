/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.FormationCRUD;
import brainiacs.techevents.crud.ParticipantFormationCRUD;
import brainiacs.techevents.entities.Formation;
import brainiacs.techevents.entities.ParticipantFormation;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * FXML Controller class
 *
 * @author linda
 */
public class IAjoutFormationController implements Initializable {
    @FXML
    private AnchorPane panefornode;
    @FXML
    private JFXTextField fttitre;
    @FXML
    private JFXTextField ftetab;
    @FXML
    private JFXTextArea ftdesc;
    @FXML
    private DatePicker ftdatedebut;
    @FXML
    private DatePicker ftdatefin;
    @FXML
    private JFXTextField ftprix;
    @FXML
    private JFXButton btnvalider;
    @FXML
    private JFXButton btRtAJAff;

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void insertFormation(ActionEvent event) {
        
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Modification");
                            alert.setHeaderText(null);
                            alert.setContentText("voulez vous vraiment ajouter cette formation ? ");
                            Optional<ButtonType> action = alert.showAndWait();
                            if (action.get() == ButtonType.OK)
                            {
                                java.sql.Date datedeb=java.sql.Date.valueOf(ftdatedebut.getValue());
                                java.sql.Date datefin=java.sql.Date.valueOf(ftdatefin.getValue());

            
                                  Formation f = new Formation();
                                  f.setDescription(ftdesc.getText());
                                  f.setTitre(fttitre.getText());
                                  f.setEtablissement(ftetab.getText());
                                  f.setDate_debut(datedeb);
                                  f.setDate_fin(datefin);
                                  f.setPrix(parseFloat(ftprix.getText()));
                                  FormationCRUD fc=new FormationCRUD();
                                  
        String videtitre="";
        String videetab="";
        String videdesc="";
        //String videdatedeb="";
        //String videdatefin="";
        //String videprix="";
        boolean test=false;
        
        FormationCRUD fcc = new FormationCRUD();
        if(fttitre.getText().equals(""))
        {
            test=true;
            videtitre=" Ce champs est vide !! "; 
        }
         if(ftetab.getText().equals(""))
        {test=true;
            videetab=" Ce champs est vide !! ";
        }
         if(ftdesc.getText().equals(""))
        {test=true;
        videdesc=" Ce champs est vide !! ";}
        /* if(ftprix.getText().equals(""))
        {test=true;
            videprix=" Ce champs est vide !! ";
        }
        */
        if(test)
        {  
            fttitre.setText(videtitre);
            ftetab.setText(videetab);
            ftdesc.setText(videdesc);
           // ftprix.setText(videprix);
        
        
        }
        else
        {
         fcc.ajouterFormation(f);
                                     try {

                     /*           FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
                                Parent root = loader.load();
                               IAfficherFormationController rc = loader.getController();
                               btnvalider.getScene().setRoot(root);   */
                                      setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));

                                         } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
               ////////Envoi d'un mail                        
            try{
            String host ="smtp.gmail.com" ;
            String user = "linda.mbarki@esprit.tn";
            String pass = "183JFT1165";
            
            ParticipantFormationCRUD p = new ParticipantFormationCRUD();
                String to = "" ;
                for (ParticipantFormation col : p.selectAllParticipants()) {
                    to = col.getMail() ;
                }
          
            String from = "linda.mbarki@esprit.tn";  
            String subject = "Une nouvelle formation ajoutée !";
            String messageText = "Soyez le bienvenue à la formation "+fttitre.getText()+"\n Veuillez visitez notre site web pour plus d'information";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new java.util.Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           System.out.println("message send successfully");
        }catch(Exception ex)
        {
            System.out.println(ex);
        }
       
    
}
    }
    
    }
    
    @FXML
    private void retournerAjoutVersAffichage(ActionEvent event) {
        try {
          /*  FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"));
            Parent root= loader.load();
            IAfficherFormationController rc= loader.getController();   
             
            btRtAJAff.getScene().setRoot(root);   */
             setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/IAfficherFormation.fxml"))));
                  
                  
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
     private void setNode(Node node) {
          System.out.println("wslllllllll");
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
    
}
        
   
