/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import brainiacs.techevents.crud.ArticleCrud;
import brainiacs.techevents.entities.Article;
import static brainiacs.techevents.controller.GestionDesArticlesController.articleRecup;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class InterfaceArticleEtablissementController implements Initializable {
     @FXML
    private AnchorPane panefornode;
   @FXML
    private JFXButton btAjArticl;
    @FXML
    private TableView<Article> tableViewArticle;
    @FXML
    private TableColumn<Article, Integer> columnID;
    @FXML
    private TableColumn<Article, String> columnAuteur;
    @FXML
    private TableColumn<Article, String> columnJournal;
    @FXML
    private TableColumn<Article, String> columnTitre;
    @FXML
    private TableColumn<Article, Date> columnDate;
    @FXML
    private TableColumn<Article, Integer> columnVues;
    @FXML
    private TableColumn<Article, Float> columnRating;
     
    public static int direction;  
    
ObservableList<Article> dataArticlelist ;
    
    @FXML
    private JFXTextField tfAuteurRechercher;
    static Article articleRecup ;
   
   
    
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
         columnID.setCellValueFactory(new PropertyValueFactory<>("identifiant"));
        columnAuteur.setCellValueFactory(new PropertyValueFactory<>("auteur"));
        columnJournal.setCellValueFactory(new PropertyValueFactory<>("journal"));
        columnTitre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnVues.setCellValueFactory(new PropertyValueFactory<>("nbrvue"));
        columnRating.setCellValueFactory(new PropertyValueFactory<>("rating"));
        
         
         addButtonConsulterToTable();
        
      
        ArticleCrud ac =new ArticleCrud();
        dataArticlelist=FXCollections.observableArrayList(ac.afficherArticles());
        tableViewArticle.setItems(dataArticlelist);
    }    

    @FXML
    private void testAff(KeyEvent event) {
        Article a=new Article();
        ArticleCrud ac =new ArticleCrud();
        dataArticlelist=FXCollections.observableArrayList(ac.rechercherArticlesParAuteur(tfAuteurRechercher.getText()));
        tableViewArticle.setItems(dataArticlelist);
    }

    @FXML
    private void redirectionAjArticle(ActionEvent event) {
         try {
          /*  FXMLLoader loader=new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/AjoutArticle.fxml"));
            Parent root= loader.load();
            AjoutArticleController rc= loader.getController();  
            
            btAjArticl.getScene().setRoot(root);   */
               setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/AjoutArticle.fxml"))));
               direction=1;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());;
        }
    }
    
    private void addButtonConsulterToTable()
    {
       TableColumn<Article, Void> colBtn = new TableColumn("Consulter Article");

        Callback<TableColumn<Article, Void>, TableCell<Article, Void>> cellFactory = new Callback<TableColumn<Article, Void>, TableCell<Article, Void>>() {
            @Override
            public TableCell<Article, Void> call(final TableColumn<Article, Void> param) {
                final TableCell<Article, Void> cell = new TableCell<Article, Void>() {

                    private final JFXButton btn = new JFXButton("Consulter");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            articleRecup=getTableView().getItems().get(getIndex());
                            String path=articleRecup.getEmplacement();
                             try {
                                     ArticleCrud ac=new ArticleCrud();
                                     ac.incrimenterNbrVur(articleRecup.getIdentifiant());

                           /*     FXMLLoader loader = new FXMLLoader(getClass().getResource("/brainiacs/techevents/gui/GestionDesArticles.fxml"));
                                Parent root = loader.load();
                               GestionDesArticlesController rc = loader.getController();
                               btn.getScene().setRoot(root);  */
                                       setNode((FXMLLoader.load(getClass().getResource("/brainiacs/techevents/gui/InterfaceArticleEtablissement.fxml"))));

                            } catch (IOException ex) {
                                System.out.println(ex.getMessage());

                            }
                            
                            Runtime runtime = Runtime.getRuntime(); 
try 
{ 
runtime.exec(new String[] 
{ 
"C:\\\\Program Files (x86)\\\\Adobe\\\\Reader 9.0\\\\Reader\\\\\\\\AcroRd32.exe", 
path} ); 
} 

catch(Exception err) 

{ 
System.out.println("err = " + err); 
}  });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        tableViewArticle.getColumns().add(colBtn); 
    
    
    }
      private void setNode(Node node) {
        panefornode.getChildren().clear();
        panefornode.getChildren().add((Node) node);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5));//dure de la translation
        ft.setNode(node);
        ft.setFromValue(0.10);//dispartion 
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
}
