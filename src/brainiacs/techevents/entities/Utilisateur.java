/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

import brainiacs.techevents.crud.Password;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Utilisateur {
    private int id ;
    private String nom;
    private String prenom ;
    private String username;
    private String mail ;
    private String mot_de_passe ;
    private String date_naissance ;
    private String localisation;
    private String num_tel ;
    private String categories;
    private String photo ;
    

    
    
    public Utilisateur(){}
    
     public Utilisateur(int id, String nom, String prenom, String username, String mail, String mot_de_passe, String date_naissance, String localisation, String num_tel, String categories,String photo) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.mail = mail;
        this.mot_de_passe = mot_de_passe;
        this.date_naissance = date_naissance;
        this.localisation = localisation;
        this.num_tel = num_tel;
        this.categories = categories;
        this.photo = photo ;
    }
    public Utilisateur(int id, String nom, String prenom, String username, String mail, String mot_de_passe, String date_naissance, String localisation, String num_tel, String categories) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.mail = mail;
        this.mot_de_passe = mot_de_passe;
        this.date_naissance = date_naissance;
        this.localisation = localisation;
        this.num_tel = num_tel;
        this.categories = categories;
    }
    
   

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getUsername() {
        return username;
    }

    public String getMail() {
        return mail;
    }

    public String getMot_de_passe() {
        return mot_de_passe;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public String getLocalisation() {
        return localisation;
    }

    public String getNum_tel() {
        return num_tel;
    }

    public String getCategories() {
        return categories;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setMot_de_passe(String mot_de_passe) {
        this.mot_de_passe = mot_de_passe;
    }

    public void setDate_naissance(String date_naissance) {
        this.date_naissance = date_naissance;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public void setNum_tel(String num_tel) {
        this.num_tel = num_tel;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    
    

    @Override
    public String toString() {
        return "Utilisateur{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", username=" + username + ", mail=" + mail + ", mot_de_passe=" + mot_de_passe + ", date_naissance=" + date_naissance + ", localisation=" + localisation + ", num_tel=" + num_tel + ", categories=" + categories + '}';
    }
    
}
