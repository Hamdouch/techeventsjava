/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;



import java.util.Date;

/**
 *
 * @author user
 */
public class Article {
    private int identifiant;
    private String auteur;
    private String journal;
    private String titre;
    private java.sql.Date date;
    private String emplacement;
     java.util.Date date_complete = new java.util.Date();
     private int nbrvue;
     private float rating;
     

   

    public Article(String auteur, String journal,String titre, String emplacement) {
        this.date = new java.sql.Date(date_complete.getTime());
       this.titre=titre;
        this.auteur = auteur;
        this.journal = journal;
        this.emplacement = emplacement;
    }
    public Article()
    {   this.date = new java.sql.Date(date_complete.getTime());
}

    public int getIdentifiant() {
        return identifiant;
    }

    public String getAuteur() {
        return auteur;
    }

    public String getJournal() {
        return journal;
    }

    public Date getDate() {
        return date;
    }

    public int getNbrvue() {
        return nbrvue;
    }

    public float getRating() {
        return rating;
    }

    public void setNbrvue(int nbrvue) {
        this.nbrvue = nbrvue;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
    

    public String getEmplacement() {
        return emplacement;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }
    

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    @Override
    public String toString() {
        return "Article{" + "identifiant=" + identifiant + ", auteur=" + auteur + ", journalEntreprise=" + journal + ", date=" + date + ", emplacement=" + emplacement + '}';
    }
    
    
    
}
