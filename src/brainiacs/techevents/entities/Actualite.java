/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class Actualite {
    private int id;
    protected String titre;
    private String description;
    private String categories;
    private String user;
    private Date date;
    private String pictures;
    
    
    

    public Actualite() {
    }

    public Actualite(int id, String titre, String description, String categories, String user, Date date, String pictures) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.categories = categories;
        this.user = user;
        this.date = date;
        this.pictures = pictures;
    }

  

  
   
    public Actualite(int id, String titre,String description,String user ,Date date) {
        this.id = id;
        this.titre = titre;
        this.date =date;
        this.user=user;
        this.description=description;
        
    }
     public Actualite( String titre,String description,String user ,Date date) {
        
        this.titre = titre;
        this.date =date;
        this.user=user;
        this.description=description;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
     public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    @Override
    public String toString() {
        return "\nActualite{" + " titre=" + titre + ", description=" + description  + ", user=" + user + ", date=" + date +",Categories : "+categories+ '}';
    }

  

   

   


  


  
    

   
    
    
}

