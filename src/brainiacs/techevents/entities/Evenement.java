/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

import brainiacs.techevents.connexion.MyConnection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Asus
 */
public class Evenement {
    private int id ;

    public int getNombreParticipants() {
        return nombreParticipants;
    }

    public void setNombreParticipants(int nombreParticipants) {
        this.nombreParticipants = nombreParticipants;
    }
    int nombreParticipants;

    public void setAfficherLaSuite(String afficherLaSuite) {
        this.afficherLaSuite = afficherLaSuite;
    }
    private String title ;

    public String getAfficherLaSuite() {
        return afficherLaSuite;
    }
    private String description;
    private String localisation;
    private String etablissement;
    private float prix;
    private Date date ;
    public Type category;

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    
    
    public String afficherLaSuite="Afficher la suite";
    public String imagepath;

    public Evenement() {
    }
   

    public Evenement(int id, String title, String description, String localisation, String etablissement, Date date, Type category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.localisation = localisation;
        this.etablissement = etablissement;
        this.date = date;
        this.category = category;
    }

    public Evenement(int id, String title, String description, String localisation, String etablissement, float prix, Date date, Type category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.localisation = localisation;
        this.etablissement = etablissement;
        this.prix = prix;
        this.date = date;
        this.category = category;
    }

    public Evenement(int id, int nombreParticipants, String title, String description, String localisation, String etablissement, float prix, Date date, Type category, String imagepath) {
        this.id = id;
        this.nombreParticipants = nombreParticipants;
        this.title = title;
        this.description = description;
        this.localisation = localisation;
        this.etablissement = etablissement;
        this.prix = prix;
        this.date = date;
        this.category = category;
        this.imagepath = imagepath;
    }
    

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocalisation() {
        return localisation;
    }

    public String getEtablissement() {
        return etablissement;
    }

    public float getPrix() {
        return prix;
    }

    public Date getDate() {
        return date;
    }

    public Type getCategory() {
        return category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public void setEtablissement(String etablissement) {
        this.etablissement = etablissement;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCategory(Type category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id=" + id + ", nombreParticipants=" + nombreParticipants + ", title=" + title + ", description=" + description + ", localisation=" + localisation + ", etablissement=" + etablissement + ", prix=" + prix + ", date=" + date + ", category=" + category + ", afficherLaSuite=" + afficherLaSuite + ", imagepath=" + imagepath + '}';
    }

    

    
    
    
    
}
