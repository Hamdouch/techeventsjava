/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

/**
 *
 * @author Asus
 */
public class Pay {
    private String numCarte;
    private String Code;

    public Pay() {
    }

    public Pay(String numCarte, String Code) {
        this.numCarte = numCarte;
        this.Code = Code;
    }
    
    

    public String getNumCarte() {
        return numCarte;
    }

    public String getCode() {
        return Code;
    }

    public void setNumCarte(String numCarte) {
        this.numCarte = numCarte;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    @Override
    public String toString() {
        return "Pay{" + "numCarte=" + numCarte + ", Code=" + Code + '}';
    }
    
    
    
}
