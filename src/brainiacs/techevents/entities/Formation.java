/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

import java.sql.Date;

/**
 *
 * @author linda
 */
public class Formation {
    private int id;
    private String titre;
    private String description;
    private String etablissement;
    private Date date_debut;
    private Date date_fin;
    private float prix;
    
    public Formation(){
        
    }

    public Formation(int id, String titre, String description, String etablissement, Date date_debut, Date date_fin, float prix) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.etablissement = etablissement;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.prix = prix;
    }
    public Formation(int id, String etablissement, String titre){
        this.id = id;
        this.titre = titre;
        this.etablissement = etablissement;
    }
    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public String getEtablissement() {
        return etablissement;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEtablissement(String etablissement) {
        this.etablissement = etablissement;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Formation{" + "id=" + id + ", titre=" + titre + ", description=" + description + ", etablissement=" + etablissement + ", date_debut=" + date_debut + ", date_fin=" + date_fin + ", prix=" + prix + '}';
    }

    
    
    
    
    
}
