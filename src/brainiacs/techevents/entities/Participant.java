/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

/**
 *
 * @author Asus
 */
public class Participant {
    private int cin;
    private String nom;
    private String prenom;
    private String mail;
    private int tlph;

    public Participant() {
    }

    
    public Participant(int cin, String nom, String prenom, String mail, int tlph) {
        this.cin = cin;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tlph = tlph;
    }

    public int getCin() {
        return cin;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public int getTlph() {
        return tlph;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setTlph(int tlph) {
        this.tlph = tlph;
    }

    @Override
    public String toString() {
        return "Participant{" + "cin=" + cin + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", tlph=" + tlph + '}';
    }
    
    
}
