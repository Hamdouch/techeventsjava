/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

/**
 *
 * @author linda
 */
public class ParticipantFormation {
    private int idpart;
    private int CIN;
    private String Nom;
    private String Prenom;
    private String Mail;
    private String Adresse;
    private int NumTelephone;
    
    public ParticipantFormation(){
        
    }

    public ParticipantFormation(int CIN, String Nom, String Prenom, String Mail, String Adresse, int NumTelephone) {
        this.CIN = CIN;
        this.Nom = Nom;
        this.Prenom = Prenom;
        this.Mail = Mail;
        this.Adresse = Adresse;
        this.NumTelephone = NumTelephone;
    }

    public int getIdpart() {
        return idpart;
    }

    public void setIdpart(int idpart) {
        this.idpart = idpart;
    }
    
    public ParticipantFormation(String mail){
        this.Mail = mail;
    }
    public int getCIN() {
        return CIN;
    }

    public void setCIN(int CIN) {
        this.CIN = CIN;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String Prenom) {
        this.Prenom = Prenom;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String Adresse) {
        this.Adresse = Adresse;
    }

    public int getNumTelephone() {
        return NumTelephone;
    }

    public void setNumTelephone(int NumTelephone) {
        this.NumTelephone = NumTelephone;
    }

    @Override
    public String toString() {
        return "Participant{" + "CIN=" + CIN + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Mail=" + Mail + ", Adresse=" + Adresse + ", NumTelephone=" + NumTelephone + '}';
    }
    
    
}
