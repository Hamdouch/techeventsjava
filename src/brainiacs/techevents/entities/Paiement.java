/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.entities;

/**
 *
 * @author linda
 */
public class Paiement {
    
    private int numCarteBancaire;
    private int mdp;
    
    public Paiement(){
        
    }

    public Paiement(int numCarteBancaire, int mdp) {
        this.numCarteBancaire = numCarteBancaire;
        this.mdp = mdp;
    }

    public int getNumCarteBancaire() {
        return numCarteBancaire;
    }

    public void setNumCarteBancaire(int numCarteBancaire) {
        this.numCarteBancaire = numCarteBancaire;
    }

    public int getMdp() {
        return mdp;
    }

    public void setMdp(int mdp) {
        this.mdp = mdp;
    }

    @Override
    public String toString() {
        return "Paiement{" + "numCarteBancaire=" + numCarteBancaire + ", mdp=" + mdp + '}';
    }
    
}
