/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;

import brainiacs.techevents.entities.Article;
import brainiacs.techevents.connexion.MyConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author user
 */
public class ArticleCrud {
     public void ajouterArticle(Article a){
         MyConnection myCNX = MyConnection.getMyConnection();
        String requete="INSERT INTO article (auteur,journal,date,titre,emplacement) VALUES (?,?,?,?,?)";
       
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            
            pst.setString(1,a.getAuteur());
            pst.setString(2,a.getJournal());
            pst.setDate(3, (java.sql.Date) a.getDate());
            pst.setString(4,a.getTitre());
            pst.setString(5, a.getEmplacement());
            pst.executeUpdate();
            System.out.println("Article Ajouté à la table generale");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
         requete="INSERT INTO articlejournee (auteur,journal,date,titre,emplacement) VALUES (?,?,?,?,?)";
       
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            
            pst.setString(1,a.getAuteur());
            pst.setString(2,a.getJournal());
            pst.setDate(3, (java.sql.Date) a.getDate());
            pst.setString(4,a.getTitre());
            pst.setString(5, a.getEmplacement());
            pst.executeUpdate();
            System.out.println("Article Ajouté à la table journaliere");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void supprimerArticle(int id){
        String requete="DELETE FROM article WHERE identifiant =?";
       
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1,id);
            pst.executeUpdate();
            System.out.println("Article supprimé");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void modifierArticle(int id,String auteur,String journal,String titre,String emplacement){
         String requete="UPDATE article SET auteur=? , journal=? , titre=? , emplacement=? WHERE identifiant=?";
         MyConnection myCNX = MyConnection.getMyConnection();
         try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setString(1,auteur);
            pst.setString(2, journal );
            pst.setString(4, emplacement );
            pst.setString(3, titre );
            pst.setInt(5,id );
            pst.executeUpdate();
            System.out.println("Article Modifié");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    
    }
    public List<Article> afficherArticles (){
        String requete="select * FROM article";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Article> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete);
            while(rs.next()){
                Article a = new Article();
                a.setIdentifiant(rs.getInt(1));
                a.setAuteur(rs.getString(2));
                a.setJournal(rs.getString(3));
                a.setDate(rs.getDate(4));
                a.setTitre(rs.getString(5));
                a.setEmplacement(rs.getString(6));
                a.setNbrvue(rs.getInt(7));
           a.setRating(rs.getFloat(8));
                myList.add(a);
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return myList;
    }
    public List<Article> rechercherArticlesParAuteur (String auteur){
        
        String requete="select * FROM article where (auteur LIKE ? or journal like ? or titre like ? )";
        MyConnection myCNX = MyConnection.getMyConnection();
        String ch="%"+auteur+"%";
        ArrayList<Article> myList = new ArrayList();
        try {
            
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,ch);
             pst.setString(2,ch);
             pst.setString(3,ch);
              
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Article a = new Article();
                a.setIdentifiant(rs.getInt(1));
                a.setAuteur(rs.getString(2));
                a.setJournal(rs.getString(3));
               a.setTitre(rs.getString(5));
                a.setEmplacement(rs.getString(6));
           
                myList.add(a);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myList;
    }
    public List<Article> rechercherArticlesParJournal (String journal){
        
        String requete="select * FROM article where journal LIKE ?";
        MyConnection myCNX = MyConnection.getMyConnection();
        String ch="%"+journal+"%";
        ArrayList<Article> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,journal);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Article a = new Article();
                a.setIdentifiant(rs.getInt(1));
                a.setAuteur(rs.getString(2));
                a.setJournal(rs.getString(3));
                a.setDate(rs.getDate(4));
                a.setTitre(rs.getString(5));
                a.setEmplacement(rs.getString(6));
           
                myList.add(a);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myList;
    }
    public List<Article> rechercherArticlesParJournalEtAuteur (String journal,String auteur){
        
        String requete="select * FROM article where (journal=? and auteur =?)";
        MyConnection myCNX = MyConnection.getMyConnection();
        
        ArrayList<Article> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,journal);
             pst.setString(2,auteur);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Article a = new Article();
                a.setIdentifiant(rs.getInt(1));
                a.setAuteur(rs.getString(2));
                a.setJournal(rs.getString(3));
                a.setDate(rs.getDate(4));
                a.setTitre(rs.getString(5));
                a.setEmplacement(rs.getString(6));
           
                myList.add(a);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myList;
    }
    public void incrimenterNbrVur(int id)
    {
MyConnection myCNX = MyConnection.getMyConnection();
         String requete="UPDATE article SET nbrvue=nbrvue+1  WHERE identifiant=?";
         
         try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1,id);
           
            pst.executeUpdate();
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
       String   requete1="UPDATE articlejournee SET nbrvue=nbrvue+1  WHERE identifiant=?";
         
         try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete1);
            pst.setInt(1,id);
           
            pst.executeUpdate();
           
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
    public String articleBuzz()
    {String ch="";
             String requete="SELECT emplacement , MAX(nbrvue) FROM article";
         try {
             
             MyConnection myCNX = MyConnection.getMyConnection();
             Statement st = myCNX.getCnx().createStatement();
             ResultSet rs = st.executeQuery(requete);
     ch=rs.getString(1);
             
             
         } catch (SQLException ex) {
             Logger.getLogger(ArticleCrud.class.getName()).log(Level.SEVERE, null, ex);
         }
                 
     System.out.println(ch);
         return ch;
    }
}
