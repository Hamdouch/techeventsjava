/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;


import brainiacs.techevents.entities.ParticipantFormation;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author linda
 */
public class ParticipantFormationCRUD {

    public void ajouterParticipation(ParticipantFormation p) {

        String requete = "INSERT INTO participantformation (CIN,Nom,Prenom,Mail,Adresse,NumTelephone) VALUES (?,?,?,?,?,?)";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {

            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1, p.getCIN());
            pst.setString(2, p.getNom());
            pst.setString(3, p.getPrenom());
            pst.setString(4, p.getMail());
            pst.setString(5, p.getAdresse());
            pst.setInt(6, p.getNumTelephone());

            pst.executeUpdate();
            System.out.println("La participation est validée");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
     public void modifierParticipant(int idpart, int CIN, String Nom, String Prenom, String Mail, String Adresse, int NumTelephone) {
        String requete3 = "UPDATE participantformation SET CIN=? , Nom=? , Prenom=?, Mail=?, Adresse=?, NumTelephone=? WHERE idpart=? ";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {

            PreparedStatement pst = myCNX.getCnx()
                    .prepareStatement(requete3);

            pst.setInt(1, CIN);
            pst.setString(2, Nom);
            pst.setString(3, Prenom);
            pst.setString(4, Mail);
            pst.setString(5, Adresse);
            pst.setInt(6, NumTelephone);
            pst.setInt(7, idpart);
            pst.executeUpdate();

            System.out.println("Participant modifié ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<ParticipantFormation> afficherParticipant() {
        String requete5 = "SELECT * FROM participantformation";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<ParticipantFormation> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx()
                    .createStatement();
            ResultSet rs = st.executeQuery(requete5);
            while (rs.next()) {
                ParticipantFormation p = new ParticipantFormation();
                p.setIdpart(rs.getInt(1));
                p.setCIN(rs.getInt(2));
                p.setNom(rs.getString(3));
                p.setPrenom(rs.getString(4));
                p.setMail(rs.getString(5));
                p.setAdresse(rs.getString(6));
                p.setNumTelephone(rs.getInt(7));

                myList.add(p);
                System.out.println("Participant affiché ");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }
    public List<ParticipantFormation> chercherPrenomParticipant(String Prenom) {
        String requete = "SELECT * FROM participantformation WHERE Prenom LIKE ? ";
        MyConnection myCNX = MyConnection.getMyConnection();
        String ch = "%" + Prenom + "%";
        ArrayList<ParticipantFormation> myList = new ArrayList();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setString(1, ch);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ParticipantFormation p = new ParticipantFormation();
                
                p.setIdpart(rs.getInt(1));
                p.setCIN(rs.getInt(2));
                p.setNom(rs.getString(3));
                p.setPrenom(rs.getString(4));
                p.setMail(rs.getString(5));
                p.setAdresse(rs.getString(6));
                p.setNumTelephone(rs.getInt(7));

                myList.add(p);
                System.out.println("Prenom trouvé! ");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public ObservableList<ParticipantFormation> selectAllParticipants() {
        ObservableList<ParticipantFormation> ListeParticipant = FXCollections.observableArrayList();
        try {

            String req = "select Mail from  participantformation ";
            
            MyConnection myCNX = MyConnection.getMyConnection();
            PreparedStatement ps = myCNX.getCnx().prepareStatement(req);

            ResultSet resultat = ps.executeQuery(req);
            
            while (resultat.next()) {
                ListeParticipant.add(new ParticipantFormation(
                        resultat.getString("Mail")
                        
                       
                ));
            }
            ps.executeQuery(req);

        } catch (SQLException e) {
            System.out.println(" Erreur " + e.getMessage());
        }
        return ListeParticipant;
    }

    public void supprimerParticipant(int idpart) {
        String requete = "DELETE FROM participantformation WHERE idpart=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx()
                    .prepareStatement(requete);
            pst.setInt(1, idpart);
            pst.executeUpdate();
            System.out.println("Participant supprimé !");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }
}