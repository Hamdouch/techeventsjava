/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;

import brainiacs.techevents.entities.Actualite;

import brainiacs.techevents.connexion.MyConnection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class GestionActualite {

    public void ajouterActualite(Actualite a) {
        String requete1 = "INSERT INTO actualite  (titre,description,categories,user,date,pictures) VALUES (?,?,?,?,?,?)";
        MyConnection cnxbd = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete1);

            pst.setString(1, a.getTitre());
            pst.setString(2, a.getDescription());
            pst.setString(3, a.getCategories());
            pst.setString(4, a.getUser());
            pst.setDate(5, (Date) a.getDate());
            pst.setString(6, a.getPictures());
            pst.executeUpdate();
            System.out.println("Actualité ajouté ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void supprimerActualite(Actualite a) {
        String requete2 = "DELETE FROM actualite WHERE id=?";
        MyConnection cnxbd = MyConnection.getMyConnection();

        try {
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete2);
            pst.setInt(1, a.getId());
            pst.executeUpdate();
            System.out.println("Actualité Supprimer");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void modifierActualite(Actualite a, int id) {
        String requete3 = "UPDATE actualite SET titre=?,description=?,categories=?,user=?,pictures=?  WHERE id=?";
        MyConnection cnxbd = MyConnection.getMyConnection();

        try {
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete3);
            pst.setString(1, a.getTitre());
            pst.setString(2, a.getDescription());
            pst.setString(3, a.getCategories());
            pst.setString(4, a.getUser());
            pst.setString(5, a.getPictures());
            pst.setInt(6, id);
            pst.executeUpdate();
            System.out.println("Actualité modifié");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }
    public void modifierActualiteTitre(String titre,int id){
        try {
            String requete4 = "UPDATE actualite SET titre=? WHERE id=?";
            MyConnection cnxbd = MyConnection.getMyConnection();
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete4);
            pst.setString(1,titre);
            pst.setInt(2, id);
            pst.executeUpdate();
            System.out.println("Titre dactualite modifier");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
    }
    public void modifierActualiteDescription(String Description,int id){
        try {
            String requete5 = "UPDATE actualite SET description=? WHERE id=?";
            MyConnection cnxbd = MyConnection.getMyConnection();
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete5);
            pst.setString(1,Description);
            pst.setInt(2, id);
            pst.executeUpdate();
            System.out.println("Description dactualite modifier");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
    }
     public void modifierActualiteUser(String user,int id){
        try {
            String requete5 = "UPDATE actualite SET user=? WHERE id=?";
            MyConnection cnxbd = MyConnection.getMyConnection();
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete5);
            pst.setString(1,user);
            pst.setInt(2, id);
            pst.executeUpdate();
            System.out.println("User dactualite modifier");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
    }
      public void modifierActualiteCategories(String Categorie,int id){
        try {
            String requete5 = "UPDATE actualite SET categories=? WHERE id=?";
            MyConnection cnxbd = MyConnection.getMyConnection();
            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete5);
            pst.setString(1,Categorie);
            pst.setInt(2, id);
            pst.executeUpdate();
            System.out.println("Categories d'actualite modifier");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
    }

    public List<Actualite> afficherActualiteParCategories(String c) {
        String requete4 = "SELECT * FROM actualite where categories=?";
        MyConnection cnxbd = MyConnection.getMyConnection();
        ArrayList<Actualite> listdesActualites = new ArrayList();
        System.out.println("Affichage des Actualitées de Catégorie : " + c);
        try {

            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete4);
            pst.setString(1, c);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                
                Actualite act = new Actualite();
                act.setId(rs.getInt(1));
                act.setTitre(rs.getString(2));
                act.setDescription(rs.getString(3));
                act.setUser(rs.getString(5));
                act.setDate(rs.getDate(6));
                act.setPictures(rs.getString(7));
                listdesActualites.add(act);
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return listdesActualites;

    }
    public List<Actualite> afficherToutesLesActualites(){
        String requete5 = "SELECT * FROM actualite ";
        MyConnection cnxbd = MyConnection.getMyConnection();

        ArrayList<Actualite> listdesActualites = new ArrayList();
        System.out.println("Affichage Toutes les Actualités dispo:");
        try {

            PreparedStatement pst = cnxbd.getCnx().prepareStatement(requete5);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                
                Actualite act = new Actualite();
                act.setId(rs.getInt(1));
                act.setTitre(rs.getString(2));
                act.setDescription(rs.getString(3));
                act.setCategories(rs.getString(4));
                act.setUser(rs.getString(5));
                act.setDate(rs.getDate(6));
                act.setPictures(rs.getString(7));
                listdesActualites.add(act);
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return listdesActualites;
    }

    public List<Actualite> rechercherActualitePartitre(String titre) {
        String requete6="SELECT * FROM actualite where titre LIKE ? ";
        MyConnection cnxbd =MyConnection.getMyConnection();
        
        String ch="%"+titre+"%";
        ArrayList<Actualite> listRecherche = new ArrayList();
        System.out.println("Rechercher Actualite par Titre : "+titre);
        try {
            PreparedStatement pst =cnxbd.getCnx().prepareStatement(requete6);
            pst.setString(1, ch);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) {
                Actualite a = new Actualite();
                a.setId(rs.getInt(1));
                a.setTitre(rs.getString(2));
                a.setDescription(rs.getString(3));
                a.setCategories(rs.getString(4));
                a.setUser(rs.getString(5));
               a.setDate(rs.getDate(6));
               listRecherche.add(a);
                
            }
           
           
        } catch (SQLException ex) {
            
        }
        return listRecherche;
         }
   
    
    public List<Actualite> rechercheActualite(String type, String valeur) {//recherche par les deux String 1ere pour le choicebox 2emevaleur de textfield
        List<Actualite> myList = new ArrayList<Actualite>();
        String requete = null;
        MyConnection cnxbd =MyConnection.getMyConnection();
        try { // LES var declaré dans le try ne sont vue que dans le try, et inversement pour en dhors du try
            if (type.equals("Titre")) {
                requete = "SELECT * FROM actualite WHERE titre like '%" + valeur + "%'"; //MAJUSCULE NON OBLIGATOIRE 
            } else if (type.equals("Description")) {
                requete = "SELECT * FROM actualite WHERE description like '%" + valeur + "%'"; //MAJUSCULE NON OBLIGATOIRE 
            } else if (type.equals("Categories")) {
                requete = "SELECT * FROM actualite WHERE categories like '%" + valeur + "%'"; //MAJUSCULE NON OBLIGATOIRE 
            } else if (type.equals("User")) {
                requete = "SELECT * FROM actualite WHERE user like '%" + valeur + "%'"; //MAJUSCULE NON OBLIGATOIRE 
            } else if (type.equals("Tout")) {
                requete = "SELECT * from actualite where titre like '%" + valeur + "%' or description like '%" + valeur + "%' or categories like '%" + valeur + "%' or user like '%" + valeur + "%'"; 
            }

            Statement pst = cnxbd.getCnx().createStatement();
            ResultSet rs = pst.executeQuery(requete);
            while (rs.next()) {
                Actualite a = new Actualite();
                a.setId(rs.getInt(1));
                a.setTitre(rs.getString(2));
                a.setDescription(rs.getString(3));
                a.setCategories(rs.getString(4));
                a.setUser(rs.getString(5));
               a.setDate(rs.getDate(6));
               a.setPictures(rs.getString(7));
               
                myList.add(a);

            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return myList;

    }

}
