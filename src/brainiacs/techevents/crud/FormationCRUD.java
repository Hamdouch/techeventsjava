/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;

import brainiacs.techevents.entities.Formation;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author linda
 */
public class FormationCRUD {

    public void ajouterFormation(Formation f) {
        String requete = "INSERT INTO formation (id,titre,description,etablissement,date_debut,date_fin,prix) VALUES (?,?,?,?,?,?,?)";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1, f.getId());
            pst.setString(2, f.getTitre());
            pst.setString(3, f.getDescription());
            pst.setString(4, f.getEtablissement());
            pst.setDate(5, (java.sql.Date) f.getDate_debut());
            pst.setDate(6, (java.sql.Date) f.getDate_fin());
            pst.setFloat(7, f.getPrix());

            pst.executeUpdate();
            System.out.println("Formation ajoutée ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void supprimerFormation(int id) {
        String requete2 = "DELETE FROM formation WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx()
                    .prepareStatement(requete2);
            pst.setInt(1, id);
            pst.executeUpdate();
            System.out.println("Formation supprimée ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void modifierFormation(int id, String titre, String description, String etablissement, Date date_debut, Date date_fin, Float prix) {
        String requete3 = "UPDATE formation SET titre=? , description=?, etablissement=?, date_debut=? , date_fin=? , prix=? WHERE id=? ";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {

            PreparedStatement pst = myCNX.getCnx()
                    .prepareStatement(requete3);

            pst.setString(1, titre);
            pst.setString(2, description);
            pst.setString(3, etablissement);
            pst.setDate(4, date_debut);
            pst.setDate(5, date_fin);
            pst.setFloat(6, prix);
            pst.setInt(7, id);
            pst.executeUpdate();

            System.out.println("Formation modifiée ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Formation> afficherFormation() {
        String requete5 = "SELECT * FROM formation";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Formation> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx()
                    .createStatement();
            ResultSet rs = st.executeQuery(requete5);
            while (rs.next()) {
                Formation f = new Formation();
                f.setId(rs.getInt(1));
                f.setTitre(rs.getString(2));
                f.setDescription(rs.getString(3));
                f.setEtablissement(rs.getString(4));
                f.setDate_debut(rs.getDate(5));
                f.setDate_fin(rs.getDate(6));
                f.setPrix(rs.getFloat(7));

                myList.add(f);
                System.out.println("Formation affichée ");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public List<Formation> chercherTitreFormation(String titre) {
        String requete6 = "SELECT * FROM formation WHERE (titre LIKE ? or etablissement LIKE ? )";
        MyConnection myCNX = MyConnection.getMyConnection();
        String ch = "%" + titre + "%";
        ArrayList<Formation> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx()
                    .createStatement();
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete6);
            pst.setString(1, ch);
            pst.setString(2, ch);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Formation f = new Formation();
                f.setId(rs.getInt("id"));
                f.setTitre(rs.getString("titre"));
                f.setDescription(rs.getString("description"));
                f.setEtablissement(rs.getString("etablissement"));
                f.setDate_debut(rs.getDate(5));
                f.setDate_fin(rs.getDate(6));
                f.setPrix(rs.getFloat(7));

                myList.add(f);
                System.out.println("titre trouvé! ");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public List<Formation> chercherEtabFormation(String etablissement) {
        String requete7 = "SELECT * FROM formation WHERE etablissement=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Formation> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx()
                    .createStatement();
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete7);
            pst.setString(1, etablissement);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Formation f = new Formation();
                f.setId(rs.getInt(1));
                f.setTitre(rs.getString(2));
                f.setDescription(rs.getString(3));
                f.setEtablissement(rs.getString(4));
                f.setDate_debut(rs.getDate(5));
                f.setDate_fin(rs.getDate(6));
                f.setPrix(rs.getFloat(7));

                myList.add(f);
                System.out.println("établissement trouvé! ");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public ObservableList<Formation> selectAllFormations() {
        ObservableList<Formation> ListeFormation = FXCollections.observableArrayList();
        try {

            String req = "select id,titre,etablissement from  formation ";

            MyConnection myCNX = MyConnection.getMyConnection();
            PreparedStatement ps = myCNX.getCnx().prepareStatement(req);

            ResultSet resultat = ps.executeQuery(req);

            while (resultat.next()) {
                ListeFormation.add(new Formation(
                        resultat.getInt("id"),
                        resultat.getString("titre"),
                        resultat.getString("etablissement")
                ));
            }
            ps.executeQuery(req);

        } catch (SQLException e) {
            System.out.println(" Erreur " + e.getMessage());
        }
        return ListeFormation;
    }

}
