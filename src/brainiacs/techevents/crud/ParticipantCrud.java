/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;


import brainiacs.techevents.entities.Participant;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Asus
 */
public class ParticipantCrud {
    public void ajouterParticipant(Participant p){
        String requete="INSERT INTO participant (cin,nom,prenom,mail,tlph) VALUES (?,?,?,?,?)";
        MyConnection myCNX=MyConnection.getMyConnection();
        try {
            PreparedStatement pst=myCNX.getCnx().prepareStatement(requete); 
            pst.setInt(1,p.getCin());
            pst.setString(2,p.getNom());
            pst.setString(3,p.getPrenom());
            pst.setString(4,p.getMail());
            pst.setInt(5,p.getTlph());
            

            pst.executeUpdate();
            System.out.println ("Participation validée!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
           // ex.printStackTrace();  //test pour savoir l'erreur 
        }
        }
    
}
