/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;

import brainiacs.techevents.entities.Paiement;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author linda
 */
public class PaiementCRUD {
    public void ajouterPaiement(Paiement p){
      String requete="INSERT INTO paiement (numCarteBancaire,mdp) VALUES (?,?)";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
            pst.setInt(1,p.getNumCarteBancaire());
            pst.setInt(2, p.getMdp());
            
            
            pst.executeUpdate();
            System.out.println("Paiement de la formation est validé!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
       
    }
}
