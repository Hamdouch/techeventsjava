/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;

import brainiacs.techevents.connexion.MyConnection;
import brainiacs.techevents.entities.Evenement;
import brainiacs.techevents.entities.Pay;
import brainiacs.techevents.entities.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;

/**
 *
 * @author Asus
 */
public class EventCrud {
    public void ajouterEvent(Evenement e){
        String requete="INSERT INTO event (id,title,description,localisation,etablissement,prix,date,category,imagepath) VALUES (?,?,?,?,?,?,?,?,?)";
        MyConnection myCNX=MyConnection.getMyConnection();
        try {
            PreparedStatement pst=myCNX.getCnx().prepareStatement(requete); 
            pst.setInt(1,e.getId());
            pst.setString(2,e.getTitle());
            pst.setString(3,e.getDescription());
            pst.setString(4,e.getLocalisation());
            pst.setString(5,e.getEtablissement());
            pst.setFloat(6, e.getPrix());
            pst.setDate(7, (java.sql.Date) e.getDate());
            pst.setString(8,e.category.name());
            pst.setString(9, e.getImagepath());

            pst.executeUpdate();
            System.out.println ("événement Ajoutée!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
           // ex.printStackTrace();  //test pour savoir l'erreur 
        }
        }
    public void supprimerEvent(int id){
        String requete2="DELETE FROM event WHERE id =?";
       
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requete2);
            pst.setInt(1,id);
            pst.executeUpdate();
            System.out.println("événement supprimé");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void modifierEvent(int id, String title, String description, String localisation, String etablissement, float prix, java.sql.Date date, Type category,String imagepath){
         String requete3="UPDATE event SET title=? , description=? , localisation=? , etablissement=? , prix=? , date=?,category=?,imagepath=? WHERE id=?";
         MyConnection myCNX = MyConnection.getMyConnection();
         try {
            PreparedStatement pst3 = myCNX.getCnx().prepareStatement(requete3);
            pst3.setString(1,title);
            pst3.setString(2,description);
            pst3.setString(3,localisation);
            pst3.setString(4,etablissement);
            pst3.setFloat(5, prix);
            pst3.setDate(6, (java.sql.Date) date);
            pst3.setString(7,category.name());
            pst3.setString(8,imagepath);
            pst3.setInt(9,id);

            pst3.executeUpdate();
            System.out.println("événement Modifié");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    
    }
    public List<Evenement> afficherEvent (){
        String requete="select * FROM event";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Evenement> myEventList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete);
            while(rs.next()){
                Evenement e = new Evenement();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setLocalisation(rs.getString(4));
                e.setEtablissement(rs.getString(5));
                e.setPrix(rs.getFloat(6));
                e.setDate(rs.getDate(7));
                e.setCategory(Type.valueOf(rs.getString(8)));
                e.setNombreParticipants(rs.getInt(9));
                e.setImagepath(rs.getString(10));
              
                myEventList.add(e);
                
                System.out.println("événement Afiché!");

            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return myEventList;
    }
    
    public List<Evenement> rechercheTitreEvent (String title){
        
        String requete="select * FROM event where (title LIKE ? OR etablissement LIKE ?  OR description LIKE? OR localisation LIKE?) ";
        MyConnection myCNX = MyConnection.getMyConnection();
        String ch="%"+title+"%";
        
        ArrayList<Evenement> myEventList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,ch);
             pst.setString(2,ch);
             pst.setString(3,ch);
             pst.setString(4,ch);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Evenement e = new Evenement();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setLocalisation(rs.getString(4));
                e.setEtablissement(rs.getString(5));
                e.setPrix(rs.getFloat(6));
                e.setDate(rs.getDate(7));
           
                myEventList.add(e);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myEventList;
    }
    public List<Evenement> rechercheEtabEvent (String etablissement){
        
        String requete="select * FROM event where etablissement=? ";
        MyConnection myCNX = MyConnection.getMyConnection();
        
        ArrayList<Evenement> myEventList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setString(1,etablissement);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Evenement e = new Evenement();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setLocalisation(rs.getString(4));
                e.setEtablissement(rs.getString(5));
                e.setPrix(rs.getFloat(6));
                e.setDate(rs.getDate(7));
           
                myEventList.add(e);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myEventList;
    }
    public List<Evenement> recherchePrixEvent (float prix){
        
        String requete="select * FROM event where prix=? ";
        MyConnection myCNX = MyConnection.getMyConnection();
        
        ArrayList<Evenement> myEventList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
             PreparedStatement pst = myCNX.getCnx().prepareStatement(requete);
             pst.setFloat(1,prix);
            
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                Evenement e = new Evenement();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setLocalisation(rs.getString(4));
                e.setEtablissement(rs.getString(5));
                e.setPrix(rs.getFloat(6));
                e.setDate(rs.getDate(7));
           
                myEventList.add(e);
                
                
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return myEventList;
    }
  
    public void incrParticipant(String titre)
    {
        try {
            String requete="update event Set nombreParticipants=nombreParticipants+1 where title=?";
            MyConnection mycnx=MyConnection.getMyConnection();
            PreparedStatement pst=mycnx.getCnx().prepareStatement(requete);
            pst.setString(1, titre);
            pst.executeUpdate();
            System.out.println("nombre participant incrimenté");
        } catch (SQLException ex) {
          System.out.println(ex.getMessage());

        }
        
        
    }
    }
       

