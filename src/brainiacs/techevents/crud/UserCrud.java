/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brainiacs.techevents.crud;
import brainiacs.techevents.techniques.DataSource;
import brainiacs.techevents.entities.Utilisateur;
import brainiacs.techevents.connexion.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
//import javax.activation.DataSource;   


/**
 *
 * @author User
 */
public class UserCrud {
    

        static brainiacs.techevents.techniques.DataSource ds = new brainiacs.techevents.techniques.DataSource ();
    Connection con = brainiacs.techevents.techniques.DataSource.getInstance().getConnection();
    private Statement ste;
            private PreparedStatement PS;
                private Connection connection;
            


                
                 
                

    public void ajouterUtilisateur(Utilisateur U) {
        String requete = "INSERT INTO membres (id,nom,prenom,username,mail,mot_de_passe,date_naissance,localisation,num_tel,categories,photo) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        MyConnection myCNX = MyConnection.getMyConnection();
        PreparedStatement pst;
        try {
            //System.out.println("debut");
            pst = myCNX.getCnx()
                    .prepareStatement(requete);
            pst.setInt(1, U.getId());
            pst.setString(2, U.getNom());
            pst.setString(3, U.getPrenom());
            pst.setString(4, U.getUsername());
            pst.setString(5, U.getMail());
            pst.setString(6, U.getMot_de_passe());
            pst.setString(7, U.getDate_naissance());
            pst.setString(8, U.getLocalisation());
            pst.setString(9, U.getNum_tel());
            pst.setString(10, U.getCategories());
            pst.setString(11, U.getPhoto());
            pst.executeUpdate();
            System.out.println("Personne ajouté");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void supprimerUtilisateur(int id) {
        String requete2 = "DELETE FROM membres WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        try {
            PreparedStatement pst = myCNX.getCnx()
                    .prepareStatement(requete2);
            pst.setInt(1, id);  // le 1er est le num de parametre dans le requete le 2eme est sa valeur
            pst.executeUpdate();
            System.out.println("utilisateur supprimé");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void modifierUtilisateur(int id, String nom, String prenom, String username, String mail, String mot_de_passe) {
        String requete4 = "UPDATE membres SET nom=?,prenom=?,username=?,mail=?,mot_de_passe=? WHERE id=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        PreparedStatement pst;
        try {
            pst = myCNX.getCnx()
                    .prepareStatement(requete4);
            pst.setInt(6, id);
            pst.setString(1, nom);
            pst.setString(2, prenom);
            pst.setString(3, username);
            pst.setString(4, mail);
            pst.setString(5, mot_de_passe);

            pst.executeUpdate();
            System.out.println("Utilisateur modifié");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Utilisateur> afficherUtilisateur() {
        String requete5 = "select * FROM membres";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Utilisateur> myList = new ArrayList();
        try {
            Statement st = myCNX.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete5);
            while (rs.next()) {
                Utilisateur u = new Utilisateur();
                u.setId(rs.getInt(1));
                u.setNom(rs.getString(2));
                u.setPrenom(rs.getString(3));
                u.setUsername(rs.getString(4));
                u.setMail(rs.getString(5));
                u.setMot_de_passe(rs.getString(6));
                u.setDate_naissance(rs.getString(7));
                u.setLocalisation(rs.getString(8));
                u.setNum_tel(rs.getString(9));
                u.setCategories(rs.getString(10));
                u.setPhoto(rs.getString(11));
                myList.add(u);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }
    public Utilisateur findbynum(String num) {
        Utilisateur ut= null;
        try 
        {
            
            PreparedStatement PS1 = con.prepareStatement("select * from membres where Num_tel =?");
            PS1.setString(1, num);
            ResultSet rs = PS1.executeQuery();
            while (rs.next()) {
                
                 ut = new Utilisateur(rs.getInt("id"),rs.getString("nom"), rs.getString("prenom"), rs.getString("username"), rs.getString("mail"), rs.getString("mot_de_passe"), rs.getString("date_naissance"), rs.getString("localisation"), rs.getString("num_tel"), rs.getString("categories"));
                
                }
        } 
        catch (SQLException e) 
        {
           System.out.println("aaaa");
        }
        return ut;
    }
    
    public Utilisateur finduserbyUsername(String us) {
        Utilisateur ut= null;
        try {
            PreparedStatement PS1 = con.prepareStatement("select * from membres where username =?");
            PS1.setString(1, us);
            ResultSet rs = PS1.executeQuery();
            while (rs.next()) {
                
                 ut = new Utilisateur(rs.getInt("id"),rs.getString("nom"), rs.getString("prenom"), rs.getString("username"), rs.getString("mail"), rs.getString("mot_de_passe"), rs.getString("date_naissance"), rs.getString("localisation"), rs.getString("num_tel"), rs.getString("categories"),rs.getString("photo"));
                
                }

            }
         catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ut;
    }
    
    ///////////////////////////////////////
    public void modifierPic(String username, String path) {
        String requete4 = "UPDATE membres SET photo=? WHERE username=?";
        MyConnection myCNX = MyConnection.getMyConnection();
        PreparedStatement pst;
        try {
            pst = myCNX.getCnx()
                    .prepareStatement(requete4);
            
            pst.setString(1, path);
            pst.setString(2, username);
            

            pst.executeUpdate();
            System.out.println("path image modifié");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    
     public Utilisateur findbyMail(String num) {
        Utilisateur ut= null;
        try 
        {
            
            PreparedStatement PS1 = con.prepareStatement("select * from membres where mail =?");
            PS1.setString(1, num);
            ResultSet rs = PS1.executeQuery();
            while (rs.next()) {
                
                 ut = new Utilisateur(rs.getInt("id"),rs.getString("nom"), rs.getString("prenom"), rs.getString("username"), rs.getString("mail"), rs.getString("mot_de_passe"), rs.getString("date_naissance"), rs.getString("localisation"), rs.getString("num_tel"), rs.getString("categories"));
                
                }
        } 
        catch (SQLException e) 
        {
           System.out.println("aaaa");
        }
        return ut;
    }
    
    

}
